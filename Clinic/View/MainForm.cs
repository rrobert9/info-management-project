﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinic.Controller;
using Clinic.View;
using Clinic.Model;
using Clinic.Model.People;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace Clinic
{
    public partial class MainForm : Form
    {
        private ClinicController clinicController;
        private ClinicView viewer;
        private HashHelper hashHelper;

        private int loggedInUserID;


        public MainForm()
        {
            InitializeComponent();

            this.logoutForm();

            this.clinicController = new ClinicController();
            this.hashHelper = new HashHelper();
            Driver d = new Driver();
            this.viewer = d.getView();
            this.loggedInUserID = 0;

            this.tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tabControl.DrawItem += new DrawItemEventHandler(this.PageTab_DrawItem);
            this.tabControl.Selecting += new TabControlCancelEventHandler(this.PageTab_Selecting);

            this.dateTimePickerSearch.Value = DateTimePicker.MinDateTime;
            this.clearNewAppointmentBoxes();
            this.clearAppointmentBoxes();
        }

        #region Tab Enable/Disable

        /// <summary>
        /// Draw a tab page based on whether it is disabled or enabled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageTab_DrawItem(object sender, DrawItemEventArgs e)
        {
            TabControl tabControl = sender as TabControl;
            TabPage tabPage = tabControl.TabPages[e.Index];

            if (tabPage.Enabled == false)
            {
                using (SolidBrush brush =
                    new SolidBrush(SystemColors.GrayText))
                {
                    e.Graphics.DrawString(tabPage.Text, tabPage.Font, brush,
                        e.Bounds.X + 3, e.Bounds.Y + 3);
                }
            }
            else
            {
                using (SolidBrush brush = new SolidBrush(tabPage.ForeColor))
                {
                    e.Graphics.DrawString(tabPage.Text, tabPage.Font, brush,
                        e.Bounds.X + 3, e.Bounds.Y + 3);
                }
            }
        }

        /// <summary>
        /// Cancel the selecting event if the TabPage is disabled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageTab_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPage.Enabled == false)
            {
                e.Cancel = true;
            }
        }

        #endregion

        #region Login/Logout/Tab Management

        private void logoutForm()
        {
            this.tabPageLogin.Enabled = true;
            this.tabControl.SelectedTab = this.tabPageLogin;

            this.tabPageRegister.Enabled = false;
            this.tabPageView.Enabled = false;
            this.tabPageNewAppointment.Enabled = false;
            this.tabPageUpdate.Enabled = false;
            this.tabPageAppointment.Enabled = false;
            this.tabPageDiagnose.Enabled = false;
            this.tabPageTest.Enabled = false;
            this.tabPageAdmin.Enabled = false;
            this.tabPageRegisterNurse.Enabled = false;

            this.labelLoggedInName.Text = "-";
            this.groupBoxAdminLogin.Visible = false;
        }

        private void loginForm()
        {
            this.tabPageRegister.Enabled = true;
            this.tabPageView.Enabled = true;
            this.tabPageNewAppointment.Enabled = true;
            this.tabPageUpdate.Enabled = true;
            this.tabPageAppointment.Enabled = true;
            this.tabPageAdmin.Enabled = false;
            this.tabPageDiagnose.Enabled = false;
            this.tabPageTest.Enabled = false;
            this.tabPageRegisterNurse.Enabled = false;

            this.tabControl.SelectedTab = this.tabPageRegister;
        }

        private void loginFormAdmin()
        {
            this.tabPageRegister.Enabled = false;
            this.tabPageView.Enabled = false;
            this.tabPageNewAppointment.Enabled = false;
            this.tabPageUpdate.Enabled = false;
            this.tabPageAppointment.Enabled = false;
            this.tabPageAdmin.Enabled = true;
            this.tabPageRegisterNurse.Enabled = true;
            
            this.tabPageDiagnose.Enabled = false;
            this.tabPageTest.Enabled = false;

            this.tabControl.SelectedTab = this.tabPageAdmin;
        }

        private void enterTestDiagnose()
        {
            this.tabPageLogin.Enabled = false;
            this.tabPageRegister.Enabled = false;
            this.tabPageView.Enabled = false;
            this.tabPageNewAppointment.Enabled = false;
            this.tabPageUpdate.Enabled = false;
            this.tabPageAdmin.Enabled = false;

            
        }

        private void exitTestDiagnose()
        {
            this.tabPageLogin.Enabled = false;
            this.tabPageRegister.Enabled = true;
            this.tabPageView.Enabled = true;
            this.tabPageNewAppointment.Enabled = true;
            this.tabPageUpdate.Enabled = true;

            this.tabPageTest.Enabled = false;
            this.tabPageAdmin.Enabled = false;
            this.tabPageDiagnose.Enabled = false;

            this.tabControl.SelectTab(this.tabPageAppointment);
        }

        private void linkLabelLogOut_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.logoutForm();
            this.clearAppointmentBoxes();
            this.clearNewAppointmentBoxes();
            this.clearNurseRegistrationBoxes();
            this.clearPatientRegistrationBoxes();
            this.clearTestDiagnoseBoxes();
            this.clearUpdateBoxes();
            this.clearViewPatientBoxes();
            this.clearAdminBoxes();
        }

        #endregion

        #region Clear Form Attributes

        private void clearLoginBoxes()
        {
            this.textBoxUsername.Clear();
            this.textBoxPassword.Clear();

            this.textBoxAdminUsername.Clear();
            this.textBoxAdminPassword.Clear();

            this.labelLoginFailed.Visible = false;
            this.tabPageLogin.Enabled = false;
        }

        private void clearPatientRegistrationBoxes()
        {
            this.textBoxfname.Clear();
            this.textBoxlname.Clear();
            this.textBoxAddress.Clear();
            this.textBoxCity.Clear();
            this.comboBoxState.Text = "";
            this.textBoxZip.Clear();
            this.textBoxPhoneNumber.Clear();

            this.labelPhoneNumberError.Visible = false;
            this.labelZipError.Visible = false;

            this.labelRegisterStatus.Visible = false;
        }

        private void clearNurseRegistrationBoxes()
        {
            this.textBoxRegisterNurseFirstName.Clear();
            this.textBoxRegisterNurseLastName.Clear();
            this.textBoxRegisterNurseAddress.Clear();
            this.textBoxRegisterNurseCity.Clear();
            this.comboBoxRegisterNurseState.Text = "";
            this.textBoxRegisterNurseZipCode.Clear();
            this.textBoxRegisterNursePhoneNumber.Clear();
            this.textBoxRegisterNursePassword.Clear();

            this.labelRegisterNursePhoneNumberError.Visible = false;
            this.labelRegisterNurseZipCodeError.Visible = false;

            this.labelRegisterNurseStatus.Visible = false;
        }

        private void clearNewAppointmentBoxes()
        {
            this.textBoxPatientIDNewAppointment.Text = "";
            this.richTextBoxAppointmentPurpose.Text = "";
            this.textBoxDoctorID.Text = "";
            this.dateTimePickerNewAppointmentDate.Value = DateTime.Today;
            this.dateTimePickerNewAppointmentTime.Value = new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                DateTime.Today.Day, 0, 0, 0);
            this.labelScheduleAppointmentStatus.Visible = false;
        }

        private void clearViewPatientBoxes()
        {
            this.textBoxSearchFullName.Clear();
            this.dateTimePickerSearch.Value = DateTime.Today;
        }

        private void clearAppointmentBoxes()
        {
            this.textBoxPatientIDAppointment.Clear();
            this.dateTimePickerAppointmentDate.Value = DateTime.Today;
            this.dateTimePickerAppointmentTime.Value = new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                DateTime.Today.Day, 0, 0, 0);
            this.labelAppointmentStatus.Visible = false;
            this.dataGridViewAppointment.DataSource = "";
            this.radioButtonSearchByIDAndDate.Checked = true;

            this.groupBoxVitalSigns.Enabled = false;
            this.numericUpDownBPDiastolic.Value = 0;
            this.numericUpDownBPSystolic.Value = 0;
            this.numericUpDownBodyTemp.Value = 0;
            this.numericUpDownPulse.Value = 0;
            this.richTextBoxSymptoms.Clear();

            this.dataGridViewLabTest.DataSource = "";
            this.dataGridViewOrderTests.DataSource = "";

            this.richTextBoxDiagnosis.Clear();

        }

        private void clearUpdateBoxes()
        {
            this.textBoxUpdatefname.Clear();
            this.textBoxUpdatelname.Clear();
            this.textBoxUpdateAddress.Clear();
            this.textBoxUpdateCity.Clear();
            this.comboBoxUpdateState.Text = "";
            this.textBoxUpdateZip.Clear();
            this.textBoxUpdatePhoneNumber.Clear();
            this.textBoxUpdateID.Clear();

            this.labelUpdateZipError.Visible = false;
            this.labelUpdatePhoneNumberError.Visible = false;

            this.groupBoxUpdatePatientInfo.Enabled = false;
        }

        private void clearTestDiagnoseBoxes()
        {
            this.labelRecordLabResultsStatus.Visible = false;
            this.labelOrderTestStatus.Visible = false;
            this.labelDiagnosisStatus.Visible = false;

            this.labelOrderTestStatus.Visible = false;
            this.labelRecordLabResultsStatus.Visible = false;

            this.dataGridViewOrderTests.DataSource = "";
            this.dataGridViewLabTest.DataSource = "";
            this.richTextBoxTestResults.Clear();

            this.richTextBoxDiagnosis.Clear();
            this.labelDiagnosisStatus.Visible = false;
        }

        private void clearAdminBoxes()
        {
            this.richTextBoxAdminSQL.Clear();
            try
            {
                this.dataGridViewAdmin.Columns.Clear();
                this.dataGridViewAdmin.Rows.Clear(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion

        #region Tab Page Login

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                int username = Convert.ToInt32(this.textBoxUsername.Text);
                byte[] password = this.hashHelper.GetSHA1(this.textBoxPassword.Text);

                Employee loggedInEmployee = this.clinicController.login(username, password);

                if (loggedInEmployee.ID == 0)
                {
                    this.textBoxPassword.Clear();
                    this.labelLoginFailed.Visible = true;
                    this.labelLoginFailed.Text = "Login Failed. Username/Password not found.";
                }
                else if (loggedInEmployee.ID == username)
                {
                    this.labelLoggedInName.Text = loggedInEmployee.FirstName;
                    this.loginForm();

                    this.loggedInUserID = username;
                    this.clearLoginBoxes();
                }
                else
                {
                    this.textBoxPassword.Clear();
                    this.labelLoginFailed.Visible = true;
                    this.labelLoginFailed.Text = "Login Failed. Username/Password not found.";
                }

            }
            catch (MySqlException mex)
            {
                this.labelLoginFailed.Visible = true;
                this.labelLoginFailed.Text = "Login Failed. Invalid Username/Password.";
            }
            catch (FormatException fe)
            {
                this.labelLoginFailed.Visible = true;
                this.labelLoginFailed.Text = "Login Failed. Invalid Username/Password.";
            }
          
        }



        private void linkLabelAdminLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.groupBoxAdminLogin.Visible = true;
        }

        private void linkLabelHideAdminLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.groupBoxAdminLogin.Visible = false;
        }

        private void buttonAdminSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int username = Convert.ToInt32(this.textBoxAdminUsername.Text);
                byte[] password = this.hashHelper.GetSHA1(this.textBoxAdminPassword.Text);

                Employee loggedInAdmin = this.clinicController.loginAdmin(username, password);

                if (loggedInAdmin.ID == 0)
                {
                    this.textBoxAdminPassword.Clear();
                    this.labelLoginFailed.Visible = true;
                    this.labelLoginFailed.Text = "Login Failed. Username/Password not found.";
                }
                else if (loggedInAdmin.ID == username)
                {
                    this.labelLoggedInName.Text = loggedInAdmin.FirstName;
                    this.loginFormAdmin();

                    this.clearLoginBoxes();
                }
                else
                {
                    this.textBoxAdminPassword.Clear();
                    this.labelLoginFailed.Visible = true;
                    this.labelLoginFailed.Text = "Login Failed. Username/Password not found.";
                }
            }
            catch (FormatException)
            {
                this.labelLoginFailed.Visible = true;
                this.labelLoginFailed.Text = "Login failed. Invalid username detected.";
            }
            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion

        #region Tab Page Register Patient

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            this.labelZipError.Visible = false;
            this.labelPhoneNumberError.Visible = false;

            if (this.textBoxPhoneNumber.Text.Length != 10)
            {
                this.textBoxPhoneNumber.Text = "";
                this.labelPhoneNumberError.Text = "Must be 10 characters long";
                this.labelPhoneNumberError.Visible = true;
            }
            else if (!Regex.IsMatch(this.textBoxPhoneNumber.Text, @"^\d+$"))
            {
                this.textBoxPhoneNumber.Text = "";
                this.labelPhoneNumberError.Text = "Must Contain Numbers only";
                this.labelPhoneNumberError.Visible = true;
            }
            else if (!Regex.IsMatch(this.textBoxZip.Text, @"^\d+$"))
            {
                this.textBoxZip.Text = "";
                this.labelZipError.Text = "Must Contain Numbers Only";
                this.labelZipError.Visible = true;
            }
            else
            {
                try
                {
                    string address = this.textBoxAddress.Text + "\t" + this.textBoxCity.Text + "\t" +
                                     this.comboBoxState.Text
                                     + "\t" + this.textBoxZip.Text;

                    Patient patient = new Patient()
                    {
                        FirstName = this.textBoxfname.Text,
                        LastName = this.textBoxlname.Text,
                        Address = address,
                        DOB = this.dateTimePicker.Value.Date,
                        PhoneNumber = this.textBoxPhoneNumber.Text
                    };

                    this.clinicController.addPatient(patient);

                    this.labelRegisterStatus.Text = "Patient successfully registered";
                    

                    this.labelRegisterStatus.Visible = true;
                    this.clearPatientRegistrationBoxes();
                }
                catch (MySqlException mex)
                {
                    this.labelRegisterStatus.Visible = true;
                    this.labelRegisterStatus.Text = "Failed to register patient";
                }
                catch (Exception ex)
                {
                    this.labelRegisterStatus.Visible = true;
                    this.labelRegisterStatus.Text = "Failed to register patient";
                }
            }
        }

        #endregion

        #region Tab Page Update Patient Info

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (this.textBoxUpdatePhoneNumber.Text.Length != 10)
            {
                this.textBoxUpdatePhoneNumber.Text = "";
                this.labelUpdatePhoneNumberError.Visible = true;
            }
            else
            {
                var id = 0;

                try
                {
                    id = Convert.ToInt32(this.textBoxUpdateID.Text);

                    string addr = this.textBoxUpdateAddress.Text + "\t" + this.textBoxUpdateCity.Text + "\t" +
                                  this.comboBoxUpdateState.Text + "\t" + this.textBoxUpdateZip.Text;

                    Patient patient = new Patient()
                    {
                        ID = id,
                        FirstName = this.textBoxUpdatefname.Text,
                        LastName = this.textBoxUpdatelname.Text,
                        Address = addr,
                        DOB = this.dateTimePickerUpdateDOB.Value.Date,
                        PhoneNumber = this.textBoxUpdatePhoneNumber.Text
                    };

                    bool updated = this.clinicController.UpdatePatient(patient);

                    if (updated)
                    {
                        this.labelUpdateStatus.Text = "Patient successfully updated";
                    }
                    else
                    {
                        this.labelUpdateStatus.Text =
                            "Failed to update patient. Please make sure all fields are filled.";
                    }
                    this.labelUpdateStatus.Visible = true;
                    this.clearUpdateBoxes();
                }
                catch (FormatException)
                {
                    return;
                }
                catch (MySqlException mex)
                {
                    this.labelUpdateStatus.Text =
                            "Failed to update patient. Please make sure all fields are filled.";
                    Console.WriteLine();
                }
            }
        }

        private void buttonUpdateSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(this.textBoxUpdateID.Text);
                Patient current = this.clinicController.GetPatientByID(id);

                this.textBoxUpdatefname.Text = current.FirstName;
                this.textBoxUpdatelname.Text = current.LastName;

                string[] addressAttributes = current.Address.Split('\t');

                this.textBoxUpdateAddress.Text = addressAttributes[0];
                this.textBoxUpdateCity.Text = addressAttributes[1];
                this.comboBoxUpdateState.Text = addressAttributes[2];
                this.textBoxUpdateZip.Text = addressAttributes[3];

                this.dateTimePickerUpdateDOB.Value = current.DOB;
                this.textBoxUpdatePhoneNumber.Text = current.PhoneNumber;
                this.labelUpdateStatus.Visible = false;

                this.groupBoxUpdatePatientInfo.Enabled = true;
            }
            catch (FormatException)
            {
                this.textBoxUpdateID.Clear();
                this.labelUpdateState.Visible = true;
                this.labelUpdateStatus.Text = "Invalid ID";
            }
            catch (NullReferenceException)
            {
                this.textBoxUpdateID.Clear();
                this.labelUpdateState.Visible = true;
                this.labelUpdateStatus.Text = "Invalid ID";
            }
            catch (IndexOutOfRangeException)
            {
                this.labelUpdateState.Visible = true;
                this.labelUpdateStatus.Text = "Encountered a Problem";
            }
            catch (MySqlException mex)
            {
                this.labelUpdateState.Visible = true;
                this.labelUpdateStatus.Text = "Encountered a Problem";
                Console.WriteLine(mex.Message);
            }
        }

        #endregion

        #region Tab Page View Pateint

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dob = this.dateTimePickerSearch.Value;
                string[] fullName = this.textBoxSearchFullName.Text.Split(' ');

                if (textBoxSearchFullName.Text == "")
                {
                    dataGridViewSearch.DataSource = this.clinicController.GetPatientByDOB(dob);
                }
                else if (fullName.Length == 2 && dateTimePickerSearch.Value == DateTimePicker.MinDateTime)
                {
                    dataGridViewSearch.DataSource = this.clinicController.GetPatientByName(fullName[0], fullName[1]);
                }
                else if (fullName.Length == 2 && dateTimePickerSearch.Value != DateTimePicker.MinDateTime)
                {
                    dataGridViewSearch.DataSource = this.clinicController.GetPatientByDOBAndName(dob, fullName[0],
                        fullName[1]);
                }
            }
            catch (MySqlException)
            {
            }
        }

        private void radioButtonSearchByName_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByName.Checked)
            {
                this.textBoxSearchFullName.Clear();
                this.labelFullName.Visible = true;
                this.textBoxSearchFullName.Visible = true;

                this.labelDatePickerSearchDOB.Visible = false;
                this.dateTimePickerSearch.Visible = false;
                this.dateTimePickerSearch.Value = DateTimePicker.MinDateTime;
            }
        }

        private void radioButtonSearchByDate_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByDate.Checked)
            {
                this.textBoxSearchFullName.Clear();
                this.labelFullName.Visible = false;
                this.textBoxSearchFullName.Visible = false;

                this.labelDatePickerSearchDOB.Visible = true;
                this.dateTimePickerSearch.Visible = true;
                this.dateTimePickerSearch.Value = DateTime.Today.Date;
            }
        }

        private void radioButtonSearchByNameAndDate_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByNameAndDate.Checked)
            {
                this.textBoxSearchFullName.Clear();
                this.labelFullName.Visible = true;
                this.textBoxSearchFullName.Visible = true;

                this.labelDatePickerSearchDOB.Visible = true;
                this.dateTimePickerSearch.Visible = true;
                this.dateTimePickerSearch.Value = DateTime.Today.Date;
            }
        }

        #endregion

        #region Tab Page Schedule New Appointment

        private void buttonScheduleAppointment_Click(object sender, EventArgs e)
        {
            try
            {
                int patientID = Convert.ToInt32(this.textBoxPatientIDNewAppointment.Text);
                int doctorID = Convert.ToInt32(this.textBoxDoctorID.Text);
                DateTime appointmentDate = this.dateTimePickerNewAppointmentDate.Value.Date +
                                           this.dateTimePickerNewAppointmentTime.Value.TimeOfDay;
                string purpose = this.richTextBoxAppointmentPurpose.Text;

                Appointment appt = new Appointment()
                {
                    PatientID = patientID,
                    DoctorID = doctorID,
                    NurseID = this.loggedInUserID,
                    Date = appointmentDate,
                    Purpose = purpose
                };

                this.clinicController.addApointment(appt);

               
                this.clearNewAppointmentBoxes();
                this.labelScheduleAppointmentStatus.Visible = true;
                this.labelScheduleAppointmentStatus.Text = "Appointment Successfully Scheduled.";
                
            }
            catch (OverflowException)
            {
                this.labelScheduleAppointmentStatus.Visible = true;
                this.labelScheduleAppointmentStatus.Text = "Failed. Invalid input Detected.";
            }
            catch (FormatException)
            {
                this.labelScheduleAppointmentStatus.Visible = true;
                this.labelScheduleAppointmentStatus.Text = "Failed. Invalid input Detected.";
            }
            catch (MySqlException mex)
            {
                this.labelScheduleAppointmentStatus.Visible = true;
                this.labelScheduleAppointmentStatus.Text = "Failed. Invalid input Detected.";
            }
            catch (Exception ex)
            {
                this.labelScheduleAppointmentStatus.Visible = true;
                this.labelScheduleAppointmentStatus.Text = "Failed. Invalid input Detected.";
            }
        }

        #endregion

        #region Tab Page Appointment

        private void radioButtonSearchByID_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByID.Checked)
            {
                this.labelPatientIDAppointment.Visible = true;
                this.labelAppointmentDate.Visible = false;
                this.labelAppointmentTime.Visible = false;

                this.textBoxPatientIDAppointment.Visible = true;
                this.dateTimePickerAppointmentDate.Visible = false;
                this.dateTimePickerAppointmentTime.Visible = false;
            }
        }

        private void radioButtonSearchByIDAndDate_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByIDAndDate.Checked)
            {
                this.labelPatientIDAppointment.Visible = true;
                this.labelAppointmentDate.Visible = true;
                this.labelAppointmentTime.Visible = true;

                this.textBoxPatientIDAppointment.Visible = true;
                this.dateTimePickerAppointmentDate.Visible = true;
                this.dateTimePickerAppointmentTime.Visible = true;
            }
        }

        private void radioButtonSearchByApptDate_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButtonSearchByApptDate.Checked)
            {
                this.labelPatientIDAppointment.Visible = false;
                this.labelAppointmentDate.Visible = true;
                this.labelAppointmentTime.Visible = true;

                this.textBoxPatientIDAppointment.Visible = false;
                this.dateTimePickerAppointmentDate.Visible = true;
                this.dateTimePickerAppointmentTime.Visible = true;
            }
        }

        private void buttonSearchAppointment_Click(object sender, EventArgs e)
        {
            try
            {
                this.labelUpdateAppointmentStatus.Visible = false;
                this.groupBoxVitalSigns.Enabled = false;

               
                DateTime appointmentDateTime = this.dateTimePickerAppointmentDate.Value.Date +
                                               this.dateTimePickerAppointmentTime.Value.TimeOfDay;

                if (this.radioButtonSearchByID.Checked)
                {
                    int patientID = Convert.ToInt32(this.textBoxPatientIDAppointment.Text);
                    this.dataGridViewAppointment.DataSource = this.clinicController.GetAppointmentByPatientID(patientID);
                }
                else if (this.radioButtonSearchByIDAndDate.Checked)
                {
                    int patientID = Convert.ToInt32(this.textBoxPatientIDAppointment.Text);
                    this.dataGridViewAppointment.DataSource = this.clinicController.GetAppointmentByPatientIDAndDate(patientID, appointmentDateTime);
                }
                else if (this.radioButtonSearchByApptDate.Checked)
                {
                    this.dataGridViewAppointment.DataSource = this.clinicController.GetAppointmentByDate(appointmentDateTime);
                }

                
                this.dataGridViewAppointment.Columns[0].Visible = false;
                this.dataGridViewAppointment.Columns[5].Visible = false;
                this.dataGridViewAppointment.Columns[6].Visible = false;
                this.dataGridViewAppointment.Columns[7].Visible = false;
                this.dataGridViewAppointment.Columns[8].Visible = false;
                this.dataGridViewAppointment.Columns[9].Visible = false;
                this.dataGridViewAppointment.Columns[10].Visible = false;
                this.dataGridViewAppointment.Columns[11].Visible = false;
                this.dataGridViewAppointment.Columns[12].Visible = false;
                 


                this.labelAppointmentStatus.Visible = true;
                this.labelAppointmentStatus.Text = "Double-click a row to select an appointment.";
            }
            catch (MySqlException mex)
            {
                this.labelAppointmentStatus.Visible = true;
                this.labelAppointmentStatus.Text = "Unable to find appointment";
                Console.WriteLine(mex.Message);
            }
            catch (FormatException fex)
            {
                this.labelAppointmentStatus.Visible = true;
                this.labelAppointmentStatus.Text = "Invalid input detected";
                Console.WriteLine(fex.Message);
            }
        }

        

        private void dataGridViewAppointment_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;

            this.numericUpDownBPSystolic.Value = appt.BPSystolic;
            this.numericUpDownBPDiastolic.Value = appt.BPDiastolic;
            this.numericUpDownBodyTemp.Value = Convert.ToDecimal(appt.BodyTemp);
            this.numericUpDownPulse.Value = appt.Pulse;
            this.richTextBoxSymptoms.Text = appt.Symptoms;

            this.groupBoxVitalSigns.Enabled = true;
            this.labelUpdateAppointmentStatus.Visible = false;
        }

        private void buttonRecord_Click(object sender, EventArgs e)
        {
            try
            {
                this.labelUpdateAppointmentStatus.Visible = false;
                var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;

                appt.BPSystolic = Convert.ToInt32(this.numericUpDownBPSystolic.Value);
                appt.BPDiastolic = Convert.ToInt32(this.numericUpDownBPDiastolic.Value);
                appt.BodyTemp = Convert.ToDouble(this.numericUpDownBodyTemp.Value);
                appt.Pulse = Convert.ToInt32(this.numericUpDownPulse.Value);
                appt.Symptoms = this.richTextBoxSymptoms.Text;

                var updated = this.clinicController.UpdateAppointment(appt);

                if (updated)
                {
                    this.labelUpdateAppointmentStatus.Visible = true;
                    this.labelUpdateAppointmentStatus.Text = "Success";
                }
            }
            catch (MySqlException mex)
            {
                Console.Write(mex.Message);
                this.labelUpdateAppointmentStatus.Visible = true;
                this.labelUpdateAppointmentStatus.Text = "Failed";
            }
        }

        private void buttonDiagnose_Click(object sender, EventArgs e)
        {
            this.enterTestDiagnose();

            this.tabPageDiagnose.Enabled = true;
            this.tabControl.SelectTab(this.tabPageDiagnose);

            
        }

        private void buttonOrderRecordTest_Click(object sender, EventArgs e)
        {
            this.enterTestDiagnose();

            this.tabPageTest.Enabled = true;
            this.tabControl.SelectTab(this.tabPageTest);

            this.dataGridViewOrderTests.DataSource = this.clinicController.GetAllTests();
            this.dataGridViewOrderTests.Columns[0].Visible = false;
            this.dataGridViewOrderTests.Columns[2].Visible = false;

            var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;
            this.dataGridViewLabTest.DataSource = this.clinicController.GetAllOrderedTests(appt.ID);
            this.dataGridViewLabTest.Columns[0].Visible = false;
            this.dataGridViewLabTest.Columns[2].Visible = false;
        }

        private void buttonFinish_Click(object sender, EventArgs e)
        {
            this.clearAppointmentBoxes();
            
            this.loginForm();
        }

        #endregion

        #region Tab Page Test

        private void dataGridViewLabTest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var test = (Test)this.dataGridViewLabTest.CurrentRow.DataBoundItem;
            this.labelTestName.Visible = true;
            this.labelTestName.Text = test.Name;
            this.richTextBoxTestResults.Text = test.Results;

            this.labelRecordLabResultsStatus.Visible = false;
        }

        private void buttonRecordTestResults_Click_1(object sender, EventArgs e)
        {
            try
            {
                var test = (Test)this.dataGridViewLabTest.CurrentRow.DataBoundItem;
                var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;
                var results = this.richTextBoxTestResults.Text;

                test.Results = results;


                this.clinicController.UpdateTestResults(test, appt.ID);

                this.labelRecordLabResultsStatus.Text = "Success";
                this.labelRecordLabResultsStatus.Visible = true;
            }
            catch (MySqlException mex)
            {
                this.labelRecordLabResultsStatus.Text = "Failed";
                this.labelRecordLabResultsStatus.Visible = true;
                Console.WriteLine(mex.Message);
            }
            catch (NullReferenceException nre)
            {
                this.labelRecordLabResultsStatus.Text = "Failed";
                this.labelRecordLabResultsStatus.Visible = true;
                Console.WriteLine(nre.Message);
            }
        }

        private void buttonOrderTest_Click(object sender, EventArgs e)
        {
            try
            {
                var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;

                var rowCount = this.dataGridViewOrderTests.Rows.GetRowCount(DataGridViewElementStates.Selected);
                if (rowCount > 0)
                {
                    for (int i = 0; i < rowCount; i++)
                    {
                        var test = (Test)this.dataGridViewOrderTests.SelectedRows[i].DataBoundItem;
                        this.clinicController.OrderTest(test, appt.ID);
                    }
                }

                this.dataGridViewLabTest.DataSource = this.clinicController.GetAllOrderedTests(appt.ID);
                this.dataGridViewLabTest.Columns[0].Visible = false;
                this.dataGridViewLabTest.Columns[2].Visible = false; 
            }
            catch (NullReferenceException nre)
            {
            }
            catch (MySqlException mex)
            {
            }
            
        }

        private void buttonDoneTest_Click(object sender, EventArgs e)
        {
            this.exitTestDiagnose();
            this.clearTestDiagnoseBoxes();
        }


        #endregion

        #region Tab Page Diagnose

        private void buttonRecordDiagnosis_Click(object sender, EventArgs e)
        {
            var appt = (Appointment)this.dataGridViewAppointment.CurrentRow.DataBoundItem;

            appt.InitialDiagnosis = richTextBoxDiagnosis.Text;
            appt.FinalDiagnosis = richTextBoxDiagnosis.Text;

            if(this.radioButtonInitialDiagnosis.Checked)
            {
                this.clinicController.UpdateAppointmentInitialDiagnosis(appt);

                this.labelDiagnosisStatus.Text = "Initial Diagnosis Successful!";
                this.labelDiagnosisStatus.Visible = true;
                this.richTextBoxDiagnosis.Clear();

                this.exitTestDiagnose();
            } 
            else
            {
                this.clinicController.UpdateAppointmentFinalDiagnosis(appt);

                this.labelDiagnosisStatus.Text = "Final Diagnosis Successful!";
                this.labelDiagnosisStatus.Visible = true;
                this.richTextBoxDiagnosis.Clear();

                this.loginForm();
                this.tabControl.SelectTab(this.tabPageAppointment);
            }
            
        }

        private void buttonCancelDiagnosis_Click(object sender, EventArgs e)
        {
            this.exitTestDiagnose();
            this.clearTestDiagnoseBoxes();

        }


        #endregion

        #region Tab Page Admin Query

        private void buttonGoPersonsSQL_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    this.dataGridViewAdmin.Columns.Clear();
                    this.dataGridViewAdmin.Rows.Clear();

                    using (var conn = new MySqlConnection("server=160.10.23.6; port=3306; uid=cs3230f16h;" +
                 "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
                  "ConvertZeroDateTime=true;"))
                    {
                        conn.Open();

                        using (
                            var cmd =
                                new MySqlCommand(
                                    this.richTextBoxAdminSQL.Text, conn))
                        {

                            using (var reader = cmd.ExecuteReader())
                            {
                                
                                for (int column = 0; column < reader.FieldCount; column++)
                                {
                                    DataGridViewTextBoxColumn dataColumn = new DataGridViewTextBoxColumn
                                    {
                                        Name = reader.GetName(column),
                                        HeaderText = reader.GetName(column)
                                    };

                                    dataGridViewAdmin.Columns.Add(dataColumn);
                            
                                }

                                this.dataGridViewAdmin.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                                while (reader.Read())
                                {
                                   DataGridViewRow dataRow = (DataGridViewRow)dataGridViewAdmin.Rows[0].Clone();
                                   

                                   for(var currentColumn = 0; currentColumn < reader.FieldCount; currentColumn++)
                                   {
                                       dataRow.Cells[currentColumn].Value = reader.GetValue(currentColumn);
                                   }

                                   this.dataGridViewAdmin.Rows.Add(dataRow);

                                }
                            }
                        }
                    }
                }
                catch (MySqlException mex)
                {
                    Console.WriteLine(mex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        #endregion

        #region Tab Page Register Nurse

        private void buttonRegisterNurse_Click(object sender, EventArgs e)
        {
            try
            {
                this.labelRegisterNurseZipCodeError.Visible = false;
                this.labelRegisterNursePhoneNumberError.Visible = false;

                if (this.textBoxRegisterNursePhoneNumber.Text.Length != 10)
                {
                    this.textBoxRegisterNursePhoneNumber.Text = "";
                    this.labelRegisterNursePhoneNumberError.Text = "Must be 10 characters long";
                    this.labelRegisterNursePhoneNumberError.Visible = true;
                }
                else if (!Regex.IsMatch(this.textBoxRegisterNursePhoneNumber.Text, @"^\d+$"))
                {
                    this.textBoxRegisterNursePhoneNumber.Text = "";
                    this.labelRegisterNursePhoneNumberError.Text = "Must Contain Numbers only";
                    this.labelRegisterNursePhoneNumber.Visible = true;
                }
                else if (!Regex.IsMatch(this.textBoxRegisterNurseZipCode.Text, @"^\d+$"))
                {
                    this.textBoxRegisterNurseZipCode.Text = "";
                    this.labelRegisterNurseZipCode.Text = "Must Contain Numbers Only";
                    this.labelRegisterNurseZipCodeError.Visible = true;
                }
                else
                {


                    string address = this.textBoxRegisterNurseAddress.Text + "\t" + this.textBoxRegisterNurseCity.Text + "\t" +
                                     this.comboBoxRegisterNurseState.Text
                                     + "\t" + this.textBoxRegisterNurseZipCode.Text;

                    Nurse nurse = new Nurse()
                    {
                        FirstName = this.textBoxRegisterNurseFirstName.Text,
                        LastName = this.textBoxRegisterNurseLastName.Text,
                        Address = address,
                        DOB = this.dateTimePickerRegisterNurse.Value.Date,
                        PhoneNumber = this.textBoxRegisterNursePhoneNumber.Text,

                    };

                    var added = this.clinicController.addNurse(nurse);
                    nurse.Password = this.hashHelper.GetSHA1(this.textBoxRegisterNursePassword.Text);

                    var updated = this.clinicController.updateNurse(nurse);

                    if (added && updated)
                    {
                        this.labelRegisterNurseStatus.Text = "Patient successfully registered";
                    }
                    else
                    {
                        this.labelRegisterNurseStatus.Text = "Failed to register patient";
                    }

                    this.labelRegisterNurseStatus.Visible = true;
                    this.clearNurseRegistrationBoxes();
                }

            }
            catch (MySqlException mex)
            {
                this.labelRegisterNurseStatus.Text = "Failed to register patient";
                Console.WriteLine(mex.Message);
            }
        }
            
        }

        #endregion
    }

