﻿namespace Clinic
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageLogin = new System.Windows.Forms.TabPage();
            this.linkLabelAdminLogin = new System.Windows.Forms.LinkLabel();
            this.groupBoxAdminLogin = new System.Windows.Forms.GroupBox();
            this.linkLabelHideAdminLogin = new System.Windows.Forms.LinkLabel();
            this.buttonAdminSubmit = new System.Windows.Forms.Button();
            this.labelAdminPassword = new System.Windows.Forms.Label();
            this.labelAdminUsername = new System.Windows.Forms.Label();
            this.textBoxAdminPassword = new System.Windows.Forms.TextBox();
            this.textBoxAdminUsername = new System.Windows.Forms.TextBox();
            this.labelLoginFailed = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUsername = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.tabPageRegister = new System.Windows.Forms.TabPage();
            this.labelZipError = new System.Windows.Forms.Label();
            this.textBoxZip = new System.Windows.Forms.TextBox();
            this.labelZip = new System.Windows.Forms.Label();
            this.comboBoxState = new System.Windows.Forms.ComboBox();
            this.labelState = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.labelCity = new System.Windows.Forms.Label();
            this.labelRegisterStatus = new System.Windows.Forms.Label();
            this.labelPhoneNumberError = new System.Windows.Forms.Label();
            this.labelRegister = new System.Windows.Forms.Label();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.labelDOB = new System.Windows.Forms.Label();
            this.labellname = new System.Windows.Forms.Label();
            this.labelfname = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxlname = new System.Windows.Forms.TextBox();
            this.textBoxfname = new System.Windows.Forms.TextBox();
            this.tabPageUpdate = new System.Windows.Forms.TabPage();
            this.groupBoxUpdatePatientInfo = new System.Windows.Forms.GroupBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelUpdateZip = new System.Windows.Forms.Label();
            this.labelUpdateZipError = new System.Windows.Forms.Label();
            this.labelUpdateCity = new System.Windows.Forms.Label();
            this.labelUpdatefname = new System.Windows.Forms.Label();
            this.labelUpdatelname = new System.Windows.Forms.Label();
            this.textBoxUpdatefname = new System.Windows.Forms.TextBox();
            this.textBoxUpdatelname = new System.Windows.Forms.TextBox();
            this.textBoxUpdateCity = new System.Windows.Forms.TextBox();
            this.comboBoxUpdateState = new System.Windows.Forms.ComboBox();
            this.labelUpdateDOB = new System.Windows.Forms.Label();
            this.labelUpdateAddress = new System.Windows.Forms.Label();
            this.labelUpdateState = new System.Windows.Forms.Label();
            this.labelUpdateStatus = new System.Windows.Forms.Label();
            this.textBoxUpdateZip = new System.Windows.Forms.TextBox();
            this.labelUpdatePhoneNumberError = new System.Windows.Forms.Label();
            this.dateTimePickerUpdateDOB = new System.Windows.Forms.DateTimePicker();
            this.textBoxUpdatePhoneNumber = new System.Windows.Forms.TextBox();
            this.labelUpdatePhoneNumber = new System.Windows.Forms.Label();
            this.textBoxUpdateAddress = new System.Windows.Forms.TextBox();
            this.buttonUpdateSearch = new System.Windows.Forms.Button();
            this.textBoxUpdateID = new System.Windows.Forms.TextBox();
            this.labelUpdateID = new System.Windows.Forms.Label();
            this.labelUpdate = new System.Windows.Forms.Label();
            this.tabPageView = new System.Windows.Forms.TabPage();
            this.radioButtonSearchByDate = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchByNameAndDate = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchByName = new System.Windows.Forms.RadioButton();
            this.labelFullName = new System.Windows.Forms.Label();
            this.textBoxSearchFullName = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.labelDatePickerSearchDOB = new System.Windows.Forms.Label();
            this.dateTimePickerSearch = new System.Windows.Forms.DateTimePicker();
            this.labelSearch = new System.Windows.Forms.Label();
            this.dataGridViewSearch = new System.Windows.Forms.DataGridView();
            this.tabPageNewAppointment = new System.Windows.Forms.TabPage();
            this.dateTimePickerNewAppointmentTime = new System.Windows.Forms.DateTimePicker();
            this.labelNewAppointmentTime = new System.Windows.Forms.Label();
            this.labelNewAppointment = new System.Windows.Forms.Label();
            this.labelScheduleAppointmentStatus = new System.Windows.Forms.Label();
            this.dateTimePickerNewAppointmentDate = new System.Windows.Forms.DateTimePicker();
            this.buttonScheduleAppointment = new System.Windows.Forms.Button();
            this.richTextBoxAppointmentPurpose = new System.Windows.Forms.RichTextBox();
            this.textBoxDoctorID = new System.Windows.Forms.TextBox();
            this.textBoxPatientIDNewAppointment = new System.Windows.Forms.TextBox();
            this.labelAppointmentPurpose = new System.Windows.Forms.Label();
            this.labelDoctorIDAppointment = new System.Windows.Forms.Label();
            this.labelNewAppointmentDate = new System.Windows.Forms.Label();
            this.labelPatientIDNewAppointment = new System.Windows.Forms.Label();
            this.tabPageAppointment = new System.Windows.Forms.TabPage();
            this.radioButtonSearchByApptDate = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchByIDAndDate = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchByID = new System.Windows.Forms.RadioButton();
            this.groupBoxVitalSigns = new System.Windows.Forms.GroupBox();
            this.buttonFinish = new System.Windows.Forms.Button();
            this.buttonOrderRecordTest = new System.Windows.Forms.Button();
            this.buttonDiagnose = new System.Windows.Forms.Button();
            this.labelUpdateAppointmentStatus = new System.Windows.Forms.Label();
            this.numericUpDownBPSystolic = new System.Windows.Forms.NumericUpDown();
            this.labelBPSystolic = new System.Windows.Forms.Label();
            this.labelSymptoms = new System.Windows.Forms.Label();
            this.numericUpDownBPDiastolic = new System.Windows.Forms.NumericUpDown();
            this.labelBPDiastolic = new System.Windows.Forms.Label();
            this.numericUpDownBodyTemp = new System.Windows.Forms.NumericUpDown();
            this.buttonRecord = new System.Windows.Forms.Button();
            this.richTextBoxSymptoms = new System.Windows.Forms.RichTextBox();
            this.labelBodyTemp = new System.Windows.Forms.Label();
            this.numericUpDownPulse = new System.Windows.Forms.NumericUpDown();
            this.labelPulse = new System.Windows.Forms.Label();
            this.dataGridViewAppointment = new System.Windows.Forms.DataGridView();
            this.labelAppointmentStatus = new System.Windows.Forms.Label();
            this.buttonSearchAppointment = new System.Windows.Forms.Button();
            this.textBoxPatientIDAppointment = new System.Windows.Forms.TextBox();
            this.dateTimePickerAppointmentTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAppointmentDate = new System.Windows.Forms.DateTimePicker();
            this.labelAppointmentTime = new System.Windows.Forms.Label();
            this.labelAppointmentDate = new System.Windows.Forms.Label();
            this.labelPatientIDAppointment = new System.Windows.Forms.Label();
            this.labelRecordAppointment = new System.Windows.Forms.Label();
            this.tabPageTest = new System.Windows.Forms.TabPage();
            this.groupBoxOrderTest = new System.Windows.Forms.GroupBox();
            this.labelOrderTestStatus = new System.Windows.Forms.Label();
            this.labelOrderTest = new System.Windows.Forms.Label();
            this.buttonOrderTest = new System.Windows.Forms.Button();
            this.dataGridViewOrderTests = new System.Windows.Forms.DataGridView();
            this.groupBoxRecordTestResults = new System.Windows.Forms.GroupBox();
            this.buttonDoneTest = new System.Windows.Forms.Button();
            this.labelTestName = new System.Windows.Forms.Label();
            this.labelRecordLabResultsStatus = new System.Windows.Forms.Label();
            this.buttonRecordTestResults = new System.Windows.Forms.Button();
            this.labelTestResults = new System.Windows.Forms.Label();
            this.labelSelectTest = new System.Windows.Forms.Label();
            this.dataGridViewLabTest = new System.Windows.Forms.DataGridView();
            this.richTextBoxTestResults = new System.Windows.Forms.RichTextBox();
            this.tabPageDiagnose = new System.Windows.Forms.TabPage();
            this.groupBoxDiagnosis = new System.Windows.Forms.GroupBox();
            this.labelDiagnosisStatus = new System.Windows.Forms.Label();
            this.buttonCancelDiagnosis = new System.Windows.Forms.Button();
            this.buttonRecordDiagnosis = new System.Windows.Forms.Button();
            this.richTextBoxDiagnosis = new System.Windows.Forms.RichTextBox();
            this.radioButtonFinalDiagnosis = new System.Windows.Forms.RadioButton();
            this.radioButtonInitialDiagnosis = new System.Windows.Forms.RadioButton();
            this.tabPageAdmin = new System.Windows.Forms.TabPage();
            this.dataGridViewAdmin = new System.Windows.Forms.DataGridView();
            this.labelQueryBox = new System.Windows.Forms.Label();
            this.buttonGoPersonsSQL = new System.Windows.Forms.Button();
            this.labelAdmin = new System.Windows.Forms.Label();
            this.richTextBoxAdminSQL = new System.Windows.Forms.RichTextBox();
            this.tabPageRegisterNurse = new System.Windows.Forms.TabPage();
            this.labelRegisterNursePassword = new System.Windows.Forms.Label();
            this.textBoxRegisterNursePassword = new System.Windows.Forms.TextBox();
            this.labelRegisterNurseStatus = new System.Windows.Forms.Label();
            this.labelRegisterNurseState = new System.Windows.Forms.Label();
            this.labelRegisterNurseZipCode = new System.Windows.Forms.Label();
            this.labelRegisterNursePhoneNumber = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelRegisterNurseAddress = new System.Windows.Forms.Label();
            this.labelRegisterNurseDateOfBirth = new System.Windows.Forms.Label();
            this.labelRegisterNurseCity = new System.Windows.Forms.Label();
            this.labelRegisterNurseFirstName = new System.Windows.Forms.Label();
            this.labelRegisterNurseLastName = new System.Windows.Forms.Label();
            this.comboBoxRegisterNurseState = new System.Windows.Forms.ComboBox();
            this.buttonRegisterNurse = new System.Windows.Forms.Button();
            this.labelRegisterNursePhoneNumberError = new System.Windows.Forms.Label();
            this.labelRegisterNurseZipCodeError = new System.Windows.Forms.Label();
            this.dateTimePickerRegisterNurse = new System.Windows.Forms.DateTimePicker();
            this.textBoxRegisterNurseLastName = new System.Windows.Forms.TextBox();
            this.textBoxRegisterNurseFirstName = new System.Windows.Forms.TextBox();
            this.textBoxRegisterNurseAddress = new System.Windows.Forms.TextBox();
            this.textBoxRegisterNurseCity = new System.Windows.Forms.TextBox();
            this.textBoxRegisterNursePhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxRegisterNurseZipCode = new System.Windows.Forms.TextBox();
            this.registerNurseLabel = new System.Windows.Forms.Label();
            this.labelLoggedIn = new System.Windows.Forms.Label();
            this.labelLoggedInName = new System.Windows.Forms.Label();
            this.linkLabelLogOut = new System.Windows.Forms.LinkLabel();
            this.tabControl.SuspendLayout();
            this.tabPageLogin.SuspendLayout();
            this.groupBoxAdminLogin.SuspendLayout();
            this.tabPageRegister.SuspendLayout();
            this.tabPageUpdate.SuspendLayout();
            this.groupBoxUpdatePatientInfo.SuspendLayout();
            this.tabPageView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearch)).BeginInit();
            this.tabPageNewAppointment.SuspendLayout();
            this.tabPageAppointment.SuspendLayout();
            this.groupBoxVitalSigns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBPSystolic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBPDiastolic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBodyTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPulse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAppointment)).BeginInit();
            this.tabPageTest.SuspendLayout();
            this.groupBoxOrderTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderTests)).BeginInit();
            this.groupBoxRecordTestResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLabTest)).BeginInit();
            this.tabPageDiagnose.SuspendLayout();
            this.groupBoxDiagnosis.SuspendLayout();
            this.tabPageAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmin)).BeginInit();
            this.tabPageRegisterNurse.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageLogin);
            this.tabControl.Controls.Add(this.tabPageRegister);
            this.tabControl.Controls.Add(this.tabPageUpdate);
            this.tabControl.Controls.Add(this.tabPageView);
            this.tabControl.Controls.Add(this.tabPageNewAppointment);
            this.tabControl.Controls.Add(this.tabPageAppointment);
            this.tabControl.Controls.Add(this.tabPageTest);
            this.tabControl.Controls.Add(this.tabPageDiagnose);
            this.tabControl.Controls.Add(this.tabPageAdmin);
            this.tabControl.Controls.Add(this.tabPageRegisterNurse);
            this.tabControl.Location = new System.Drawing.Point(0, 18);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(697, 355);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageLogin
            // 
            this.tabPageLogin.Controls.Add(this.linkLabelAdminLogin);
            this.tabPageLogin.Controls.Add(this.groupBoxAdminLogin);
            this.tabPageLogin.Controls.Add(this.labelLoginFailed);
            this.tabPageLogin.Controls.Add(this.labelLogin);
            this.tabPageLogin.Controls.Add(this.buttonSubmit);
            this.tabPageLogin.Controls.Add(this.labelPassword);
            this.tabPageLogin.Controls.Add(this.labelUsername);
            this.tabPageLogin.Controls.Add(this.textBoxPassword);
            this.tabPageLogin.Controls.Add(this.textBoxUsername);
            this.tabPageLogin.Location = new System.Drawing.Point(4, 22);
            this.tabPageLogin.Name = "tabPageLogin";
            this.tabPageLogin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLogin.Size = new System.Drawing.Size(689, 329);
            this.tabPageLogin.TabIndex = 0;
            this.tabPageLogin.Text = "Login";
            this.tabPageLogin.UseVisualStyleBackColor = true;
            // 
            // linkLabelAdminLogin
            // 
            this.linkLabelAdminLogin.AutoSize = true;
            this.linkLabelAdminLogin.Location = new System.Drawing.Point(7, 303);
            this.linkLabelAdminLogin.Name = "linkLabelAdminLogin";
            this.linkLabelAdminLogin.Size = new System.Drawing.Size(65, 13);
            this.linkLabelAdminLogin.TabIndex = 10;
            this.linkLabelAdminLogin.TabStop = true;
            this.linkLabelAdminLogin.Text = "Admin Login";
            this.linkLabelAdminLogin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelAdminLogin_LinkClicked);
            // 
            // groupBoxAdminLogin
            // 
            this.groupBoxAdminLogin.Controls.Add(this.linkLabelHideAdminLogin);
            this.groupBoxAdminLogin.Controls.Add(this.buttonAdminSubmit);
            this.groupBoxAdminLogin.Controls.Add(this.labelAdminPassword);
            this.groupBoxAdminLogin.Controls.Add(this.labelAdminUsername);
            this.groupBoxAdminLogin.Controls.Add(this.textBoxAdminPassword);
            this.groupBoxAdminLogin.Controls.Add(this.textBoxAdminUsername);
            this.groupBoxAdminLogin.Location = new System.Drawing.Point(343, 49);
            this.groupBoxAdminLogin.Name = "groupBoxAdminLogin";
            this.groupBoxAdminLogin.Size = new System.Drawing.Size(251, 229);
            this.groupBoxAdminLogin.TabIndex = 9;
            this.groupBoxAdminLogin.TabStop = false;
            this.groupBoxAdminLogin.Text = "Admin Login";
            this.groupBoxAdminLogin.Visible = false;
            // 
            // linkLabelHideAdminLogin
            // 
            this.linkLabelHideAdminLogin.AutoSize = true;
            this.linkLabelHideAdminLogin.Location = new System.Drawing.Point(7, 210);
            this.linkLabelHideAdminLogin.Name = "linkLabelHideAdminLogin";
            this.linkLabelHideAdminLogin.Size = new System.Drawing.Size(29, 13);
            this.linkLabelHideAdminLogin.TabIndex = 5;
            this.linkLabelHideAdminLogin.TabStop = true;
            this.linkLabelHideAdminLogin.Text = "Hide";
            this.linkLabelHideAdminLogin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelHideAdminLogin_LinkClicked);
            // 
            // buttonAdminSubmit
            // 
            this.buttonAdminSubmit.Location = new System.Drawing.Point(169, 145);
            this.buttonAdminSubmit.Name = "buttonAdminSubmit";
            this.buttonAdminSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonAdminSubmit.TabIndex = 4;
            this.buttonAdminSubmit.Text = "Submit";
            this.buttonAdminSubmit.UseVisualStyleBackColor = true;
            this.buttonAdminSubmit.Click += new System.EventHandler(this.buttonAdminSubmit_Click);
            // 
            // labelAdminPassword
            // 
            this.labelAdminPassword.AutoSize = true;
            this.labelAdminPassword.Location = new System.Drawing.Point(65, 80);
            this.labelAdminPassword.Name = "labelAdminPassword";
            this.labelAdminPassword.Size = new System.Drawing.Size(53, 13);
            this.labelAdminPassword.TabIndex = 3;
            this.labelAdminPassword.Text = "Password";
            // 
            // labelAdminUsername
            // 
            this.labelAdminUsername.AutoSize = true;
            this.labelAdminUsername.Location = new System.Drawing.Point(63, 43);
            this.labelAdminUsername.Name = "labelAdminUsername";
            this.labelAdminUsername.Size = new System.Drawing.Size(55, 13);
            this.labelAdminUsername.TabIndex = 2;
            this.labelAdminUsername.Text = "Username";
            // 
            // textBoxAdminPassword
            // 
            this.textBoxAdminPassword.Location = new System.Drawing.Point(145, 77);
            this.textBoxAdminPassword.Name = "textBoxAdminPassword";
            this.textBoxAdminPassword.PasswordChar = '*';
            this.textBoxAdminPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdminPassword.TabIndex = 1;
            // 
            // textBoxAdminUsername
            // 
            this.textBoxAdminUsername.Location = new System.Drawing.Point(145, 40);
            this.textBoxAdminUsername.Name = "textBoxAdminUsername";
            this.textBoxAdminUsername.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdminUsername.TabIndex = 0;
            // 
            // labelLoginFailed
            // 
            this.labelLoginFailed.AutoSize = true;
            this.labelLoginFailed.ForeColor = System.Drawing.Color.Red;
            this.labelLoginFailed.Location = new System.Drawing.Point(95, 165);
            this.labelLoginFailed.Name = "labelLoginFailed";
            this.labelLoginFailed.Size = new System.Drawing.Size(64, 13);
            this.labelLoginFailed.TabIndex = 8;
            this.labelLoginFailed.Text = "Login Failed";
            this.labelLoginFailed.Visible = false;
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelLogin.Location = new System.Drawing.Point(8, 7);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(245, 18);
            this.labelLogin.TabIndex = 7;
            this.labelLogin.Text = "Please Enter Your Login Information";
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(120, 126);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 4;
            this.buttonSubmit.Text = "Submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(36, 89);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 3;
            this.labelPassword.Text = "Password";
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(34, 52);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(55, 13);
            this.labelUsername.TabIndex = 2;
            this.labelUsername.Text = "Username";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(95, 86);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 1;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(95, 49);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(100, 20);
            this.textBoxUsername.TabIndex = 0;
            // 
            // tabPageRegister
            // 
            this.tabPageRegister.Controls.Add(this.labelZipError);
            this.tabPageRegister.Controls.Add(this.textBoxZip);
            this.tabPageRegister.Controls.Add(this.labelZip);
            this.tabPageRegister.Controls.Add(this.comboBoxState);
            this.tabPageRegister.Controls.Add(this.labelState);
            this.tabPageRegister.Controls.Add(this.textBoxCity);
            this.tabPageRegister.Controls.Add(this.labelCity);
            this.tabPageRegister.Controls.Add(this.labelRegisterStatus);
            this.tabPageRegister.Controls.Add(this.labelPhoneNumberError);
            this.tabPageRegister.Controls.Add(this.labelRegister);
            this.tabPageRegister.Controls.Add(this.buttonRegister);
            this.tabPageRegister.Controls.Add(this.labelPhoneNumber);
            this.tabPageRegister.Controls.Add(this.labelAddress);
            this.tabPageRegister.Controls.Add(this.dateTimePicker);
            this.tabPageRegister.Controls.Add(this.labelDOB);
            this.tabPageRegister.Controls.Add(this.labellname);
            this.tabPageRegister.Controls.Add(this.labelfname);
            this.tabPageRegister.Controls.Add(this.textBoxPhoneNumber);
            this.tabPageRegister.Controls.Add(this.textBoxAddress);
            this.tabPageRegister.Controls.Add(this.textBoxlname);
            this.tabPageRegister.Controls.Add(this.textBoxfname);
            this.tabPageRegister.Location = new System.Drawing.Point(4, 22);
            this.tabPageRegister.Name = "tabPageRegister";
            this.tabPageRegister.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRegister.Size = new System.Drawing.Size(689, 329);
            this.tabPageRegister.TabIndex = 1;
            this.tabPageRegister.Text = "Register";
            this.tabPageRegister.UseVisualStyleBackColor = true;
            // 
            // labelZipError
            // 
            this.labelZipError.AutoSize = true;
            this.labelZipError.ForeColor = System.Drawing.Color.Red;
            this.labelZipError.Location = new System.Drawing.Point(266, 204);
            this.labelZipError.Name = "labelZipError";
            this.labelZipError.Size = new System.Drawing.Size(138, 13);
            this.labelZipError.TabIndex = 24;
            this.labelZipError.Text = "Must Contain Only Numbers";
            this.labelZipError.Visible = false;
            // 
            // textBoxZip
            // 
            this.textBoxZip.Location = new System.Drawing.Point(143, 201);
            this.textBoxZip.Name = "textBoxZip";
            this.textBoxZip.Size = new System.Drawing.Size(100, 20);
            this.textBoxZip.TabIndex = 23;
            // 
            // labelZip
            // 
            this.labelZip.AutoSize = true;
            this.labelZip.Location = new System.Drawing.Point(66, 204);
            this.labelZip.Name = "labelZip";
            this.labelZip.Size = new System.Drawing.Size(53, 13);
            this.labelZip.TabIndex = 22;
            this.labelZip.Text = "Zip Code:";
            // 
            // comboBoxState
            // 
            this.comboBoxState.FormattingEnabled = true;
            this.comboBoxState.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"});
            this.comboBoxState.Location = new System.Drawing.Point(340, 174);
            this.comboBoxState.Name = "comboBoxState";
            this.comboBoxState.Size = new System.Drawing.Size(96, 21);
            this.comboBoxState.TabIndex = 21;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(295, 177);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(35, 13);
            this.labelState.TabIndex = 20;
            this.labelState.Text = "State:";
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(143, 174);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxCity.TabIndex = 19;
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(92, 177);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(27, 13);
            this.labelCity.TabIndex = 18;
            this.labelCity.Text = "City:";
            // 
            // labelRegisterStatus
            // 
            this.labelRegisterStatus.AutoSize = true;
            this.labelRegisterStatus.Location = new System.Drawing.Point(87, 278);
            this.labelRegisterStatus.Name = "labelRegisterStatus";
            this.labelRegisterStatus.Size = new System.Drawing.Size(79, 13);
            this.labelRegisterStatus.TabIndex = 17;
            this.labelRegisterStatus.Text = "Register Status";
            this.labelRegisterStatus.Visible = false;
            // 
            // labelPhoneNumberError
            // 
            this.labelPhoneNumberError.AutoSize = true;
            this.labelPhoneNumberError.ForeColor = System.Drawing.Color.Red;
            this.labelPhoneNumberError.Location = new System.Drawing.Point(266, 234);
            this.labelPhoneNumberError.Name = "labelPhoneNumberError";
            this.labelPhoneNumberError.Size = new System.Drawing.Size(141, 13);
            this.labelPhoneNumberError.TabIndex = 16;
            this.labelPhoneNumberError.Text = "Must be 10 Characters Long";
            this.labelPhoneNumberError.Visible = false;
            // 
            // labelRegister
            // 
            this.labelRegister.AutoSize = true;
            this.labelRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelRegister.Location = new System.Drawing.Point(8, 7);
            this.labelRegister.Name = "labelRegister";
            this.labelRegister.Size = new System.Drawing.Size(158, 18);
            this.labelRegister.TabIndex = 14;
            this.labelRegister.Text = "Register a New Patient";
            // 
            // buttonRegister
            // 
            this.buttonRegister.Location = new System.Drawing.Point(298, 273);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(75, 23);
            this.buttonRegister.TabIndex = 13;
            this.buttonRegister.Text = "Register";
            this.buttonRegister.UseVisualStyleBackColor = true;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Location = new System.Drawing.Point(38, 230);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(81, 13);
            this.labelPhoneNumber.TabIndex = 12;
            this.labelPhoneNumber.Text = "Phone Number:";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(71, 151);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(48, 13);
            this.labelAddress.TabIndex = 11;
            this.labelAddress.Text = "Address:";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(143, 122);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 10;
            // 
            // labelDOB
            // 
            this.labelDOB.AutoSize = true;
            this.labelDOB.Location = new System.Drawing.Point(50, 128);
            this.labelDOB.Name = "labelDOB";
            this.labelDOB.Size = new System.Drawing.Size(69, 13);
            this.labelDOB.TabIndex = 9;
            this.labelDOB.Text = "Date of Birth:";
            // 
            // labellname
            // 
            this.labellname.AutoSize = true;
            this.labellname.Location = new System.Drawing.Point(59, 97);
            this.labellname.Name = "labellname";
            this.labellname.Size = new System.Drawing.Size(61, 13);
            this.labellname.TabIndex = 8;
            this.labellname.Text = "Last Name:";
            // 
            // labelfname
            // 
            this.labelfname.AutoSize = true;
            this.labelfname.Location = new System.Drawing.Point(59, 73);
            this.labelfname.Name = "labelfname";
            this.labelfname.Size = new System.Drawing.Size(60, 13);
            this.labelfname.TabIndex = 7;
            this.labelfname.Text = "First Name:";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(143, 227);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxPhoneNumber.TabIndex = 5;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(143, 148);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(293, 20);
            this.textBoxAddress.TabIndex = 4;
            // 
            // textBoxlname
            // 
            this.textBoxlname.Location = new System.Drawing.Point(143, 94);
            this.textBoxlname.Name = "textBoxlname";
            this.textBoxlname.Size = new System.Drawing.Size(100, 20);
            this.textBoxlname.TabIndex = 2;
            // 
            // textBoxfname
            // 
            this.textBoxfname.Location = new System.Drawing.Point(143, 70);
            this.textBoxfname.Name = "textBoxfname";
            this.textBoxfname.Size = new System.Drawing.Size(100, 20);
            this.textBoxfname.TabIndex = 1;
            // 
            // tabPageUpdate
            // 
            this.tabPageUpdate.Controls.Add(this.groupBoxUpdatePatientInfo);
            this.tabPageUpdate.Controls.Add(this.buttonUpdateSearch);
            this.tabPageUpdate.Controls.Add(this.textBoxUpdateID);
            this.tabPageUpdate.Controls.Add(this.labelUpdateID);
            this.tabPageUpdate.Controls.Add(this.labelUpdate);
            this.tabPageUpdate.Location = new System.Drawing.Point(4, 22);
            this.tabPageUpdate.Name = "tabPageUpdate";
            this.tabPageUpdate.Size = new System.Drawing.Size(689, 329);
            this.tabPageUpdate.TabIndex = 4;
            this.tabPageUpdate.Text = "Update";
            this.tabPageUpdate.UseVisualStyleBackColor = true;
            // 
            // groupBoxUpdatePatientInfo
            // 
            this.groupBoxUpdatePatientInfo.Controls.Add(this.buttonUpdate);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateZip);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateZipError);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateCity);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdatefname);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdatelname);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdatefname);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdatelname);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdateCity);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.comboBoxUpdateState);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateDOB);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateAddress);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateState);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdateStatus);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdateZip);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdatePhoneNumberError);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.dateTimePickerUpdateDOB);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdatePhoneNumber);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.labelUpdatePhoneNumber);
            this.groupBoxUpdatePatientInfo.Controls.Add(this.textBoxUpdateAddress);
            this.groupBoxUpdatePatientInfo.Enabled = false;
            this.groupBoxUpdatePatientInfo.Location = new System.Drawing.Point(43, 35);
            this.groupBoxUpdatePatientInfo.Name = "groupBoxUpdatePatientInfo";
            this.groupBoxUpdatePatientInfo.Size = new System.Drawing.Size(392, 281);
            this.groupBoxUpdatePatientInfo.TabIndex = 39;
            this.groupBoxUpdatePatientInfo.TabStop = false;
            this.groupBoxUpdatePatientInfo.Text = "Patient Information";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(287, 247);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 28;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelUpdateZip
            // 
            this.labelUpdateZip.AutoSize = true;
            this.labelUpdateZip.Location = new System.Drawing.Point(34, 174);
            this.labelUpdateZip.Name = "labelUpdateZip";
            this.labelUpdateZip.Size = new System.Drawing.Size(53, 13);
            this.labelUpdateZip.TabIndex = 36;
            this.labelUpdateZip.Text = "Zip Code:";
            // 
            // labelUpdateZipError
            // 
            this.labelUpdateZipError.AutoSize = true;
            this.labelUpdateZipError.ForeColor = System.Drawing.Color.Red;
            this.labelUpdateZipError.Location = new System.Drawing.Point(245, 174);
            this.labelUpdateZipError.Name = "labelUpdateZipError";
            this.labelUpdateZipError.Size = new System.Drawing.Size(114, 13);
            this.labelUpdateZipError.TabIndex = 38;
            this.labelUpdateZipError.Text = "Must be Numbers Only";
            this.labelUpdateZipError.Visible = false;
            // 
            // labelUpdateCity
            // 
            this.labelUpdateCity.AutoSize = true;
            this.labelUpdateCity.Location = new System.Drawing.Point(60, 148);
            this.labelUpdateCity.Name = "labelUpdateCity";
            this.labelUpdateCity.Size = new System.Drawing.Size(27, 13);
            this.labelUpdateCity.TabIndex = 32;
            this.labelUpdateCity.Text = "City:";
            // 
            // labelUpdatefname
            // 
            this.labelUpdatefname.AutoSize = true;
            this.labelUpdatefname.Location = new System.Drawing.Point(27, 25);
            this.labelUpdatefname.Name = "labelUpdatefname";
            this.labelUpdatefname.Size = new System.Drawing.Size(60, 13);
            this.labelUpdatefname.TabIndex = 16;
            this.labelUpdatefname.Text = "First Name:";
            // 
            // labelUpdatelname
            // 
            this.labelUpdatelname.AutoSize = true;
            this.labelUpdatelname.Location = new System.Drawing.Point(26, 51);
            this.labelUpdatelname.Name = "labelUpdatelname";
            this.labelUpdatelname.Size = new System.Drawing.Size(61, 13);
            this.labelUpdatelname.TabIndex = 17;
            this.labelUpdatelname.Text = "Last Name:";
            // 
            // textBoxUpdatefname
            // 
            this.textBoxUpdatefname.Location = new System.Drawing.Point(104, 22);
            this.textBoxUpdatefname.Name = "textBoxUpdatefname";
            this.textBoxUpdatefname.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdatefname.TabIndex = 21;
            // 
            // textBoxUpdatelname
            // 
            this.textBoxUpdatelname.Location = new System.Drawing.Point(104, 48);
            this.textBoxUpdatelname.Name = "textBoxUpdatelname";
            this.textBoxUpdatelname.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdatelname.TabIndex = 22;
            // 
            // textBoxUpdateCity
            // 
            this.textBoxUpdateCity.Location = new System.Drawing.Point(104, 145);
            this.textBoxUpdateCity.Name = "textBoxUpdateCity";
            this.textBoxUpdateCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateCity.TabIndex = 34;
            // 
            // comboBoxUpdateState
            // 
            this.comboBoxUpdateState.FormattingEnabled = true;
            this.comboBoxUpdateState.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"});
            this.comboBoxUpdateState.Location = new System.Drawing.Point(277, 145);
            this.comboBoxUpdateState.Name = "comboBoxUpdateState";
            this.comboBoxUpdateState.Size = new System.Drawing.Size(99, 21);
            this.comboBoxUpdateState.TabIndex = 35;
            // 
            // labelUpdateDOB
            // 
            this.labelUpdateDOB.AutoSize = true;
            this.labelUpdateDOB.Location = new System.Drawing.Point(18, 99);
            this.labelUpdateDOB.Name = "labelUpdateDOB";
            this.labelUpdateDOB.Size = new System.Drawing.Size(69, 13);
            this.labelUpdateDOB.TabIndex = 18;
            this.labelUpdateDOB.Text = "Date of Birth:";
            // 
            // labelUpdateAddress
            // 
            this.labelUpdateAddress.AutoSize = true;
            this.labelUpdateAddress.Location = new System.Drawing.Point(39, 122);
            this.labelUpdateAddress.Name = "labelUpdateAddress";
            this.labelUpdateAddress.Size = new System.Drawing.Size(48, 13);
            this.labelUpdateAddress.TabIndex = 19;
            this.labelUpdateAddress.Text = "Address:";
            // 
            // labelUpdateState
            // 
            this.labelUpdateState.AutoSize = true;
            this.labelUpdateState.Location = new System.Drawing.Point(233, 148);
            this.labelUpdateState.Name = "labelUpdateState";
            this.labelUpdateState.Size = new System.Drawing.Size(35, 13);
            this.labelUpdateState.TabIndex = 33;
            this.labelUpdateState.Text = "State:";
            // 
            // labelUpdateStatus
            // 
            this.labelUpdateStatus.AutoSize = true;
            this.labelUpdateStatus.Location = new System.Drawing.Point(73, 252);
            this.labelUpdateStatus.Name = "labelUpdateStatus";
            this.labelUpdateStatus.Size = new System.Drawing.Size(75, 13);
            this.labelUpdateStatus.TabIndex = 26;
            this.labelUpdateStatus.Text = "Update Status";
            this.labelUpdateStatus.Visible = false;
            // 
            // textBoxUpdateZip
            // 
            this.textBoxUpdateZip.Location = new System.Drawing.Point(104, 171);
            this.textBoxUpdateZip.Name = "textBoxUpdateZip";
            this.textBoxUpdateZip.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateZip.TabIndex = 37;
            // 
            // labelUpdatePhoneNumberError
            // 
            this.labelUpdatePhoneNumberError.AutoSize = true;
            this.labelUpdatePhoneNumberError.ForeColor = System.Drawing.Color.Red;
            this.labelUpdatePhoneNumberError.Location = new System.Drawing.Point(245, 210);
            this.labelUpdatePhoneNumberError.Name = "labelUpdatePhoneNumberError";
            this.labelUpdatePhoneNumberError.Size = new System.Drawing.Size(141, 13);
            this.labelUpdatePhoneNumberError.TabIndex = 27;
            this.labelUpdatePhoneNumberError.Text = "Must be 10 Characters Long";
            this.labelUpdatePhoneNumberError.Visible = false;
            // 
            // dateTimePickerUpdateDOB
            // 
            this.dateTimePickerUpdateDOB.Location = new System.Drawing.Point(104, 93);
            this.dateTimePickerUpdateDOB.Name = "dateTimePickerUpdateDOB";
            this.dateTimePickerUpdateDOB.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerUpdateDOB.TabIndex = 25;
            // 
            // textBoxUpdatePhoneNumber
            // 
            this.textBoxUpdatePhoneNumber.Location = new System.Drawing.Point(104, 207);
            this.textBoxUpdatePhoneNumber.Name = "textBoxUpdatePhoneNumber";
            this.textBoxUpdatePhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdatePhoneNumber.TabIndex = 24;
            // 
            // labelUpdatePhoneNumber
            // 
            this.labelUpdatePhoneNumber.AutoSize = true;
            this.labelUpdatePhoneNumber.Location = new System.Drawing.Point(6, 210);
            this.labelUpdatePhoneNumber.Name = "labelUpdatePhoneNumber";
            this.labelUpdatePhoneNumber.Size = new System.Drawing.Size(81, 13);
            this.labelUpdatePhoneNumber.TabIndex = 20;
            this.labelUpdatePhoneNumber.Text = "Phone Number:";
            // 
            // textBoxUpdateAddress
            // 
            this.textBoxUpdateAddress.Location = new System.Drawing.Point(104, 119);
            this.textBoxUpdateAddress.Name = "textBoxUpdateAddress";
            this.textBoxUpdateAddress.Size = new System.Drawing.Size(272, 20);
            this.textBoxUpdateAddress.TabIndex = 23;
            // 
            // buttonUpdateSearch
            // 
            this.buttonUpdateSearch.Location = new System.Drawing.Point(504, 6);
            this.buttonUpdateSearch.Name = "buttonUpdateSearch";
            this.buttonUpdateSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateSearch.TabIndex = 31;
            this.buttonUpdateSearch.Text = "Search";
            this.buttonUpdateSearch.UseVisualStyleBackColor = true;
            this.buttonUpdateSearch.Click += new System.EventHandler(this.buttonUpdateSearch_Click);
            // 
            // textBoxUpdateID
            // 
            this.textBoxUpdateID.Location = new System.Drawing.Point(398, 8);
            this.textBoxUpdateID.Name = "textBoxUpdateID";
            this.textBoxUpdateID.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateID.TabIndex = 30;
            // 
            // labelUpdateID
            // 
            this.labelUpdateID.AutoSize = true;
            this.labelUpdateID.Location = new System.Drawing.Point(250, 11);
            this.labelUpdateID.Name = "labelUpdateID";
            this.labelUpdateID.Size = new System.Drawing.Size(142, 13);
            this.labelUpdateID.TabIndex = 29;
            this.labelUpdateID.Text = "Please Give the Patient\'s ID:\r\n";
            // 
            // labelUpdate
            // 
            this.labelUpdate.AutoSize = true;
            this.labelUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelUpdate.Location = new System.Drawing.Point(8, 7);
            this.labelUpdate.Name = "labelUpdate";
            this.labelUpdate.Size = new System.Drawing.Size(182, 18);
            this.labelUpdate.TabIndex = 15;
            this.labelUpdate.Text = "Update Patient Information";
            // 
            // tabPageView
            // 
            this.tabPageView.Controls.Add(this.radioButtonSearchByDate);
            this.tabPageView.Controls.Add(this.radioButtonSearchByNameAndDate);
            this.tabPageView.Controls.Add(this.radioButtonSearchByName);
            this.tabPageView.Controls.Add(this.labelFullName);
            this.tabPageView.Controls.Add(this.textBoxSearchFullName);
            this.tabPageView.Controls.Add(this.buttonSearch);
            this.tabPageView.Controls.Add(this.labelDatePickerSearchDOB);
            this.tabPageView.Controls.Add(this.dateTimePickerSearch);
            this.tabPageView.Controls.Add(this.labelSearch);
            this.tabPageView.Controls.Add(this.dataGridViewSearch);
            this.tabPageView.Location = new System.Drawing.Point(4, 22);
            this.tabPageView.Name = "tabPageView";
            this.tabPageView.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageView.Size = new System.Drawing.Size(689, 329);
            this.tabPageView.TabIndex = 2;
            this.tabPageView.Text = "View Patient";
            this.tabPageView.UseVisualStyleBackColor = true;
            // 
            // radioButtonSearchByDate
            // 
            this.radioButtonSearchByDate.AutoSize = true;
            this.radioButtonSearchByDate.Location = new System.Drawing.Point(451, 7);
            this.radioButtonSearchByDate.Name = "radioButtonSearchByDate";
            this.radioButtonSearchByDate.Size = new System.Drawing.Size(99, 17);
            this.radioButtonSearchByDate.TabIndex = 23;
            this.radioButtonSearchByDate.TabStop = true;
            this.radioButtonSearchByDate.Text = "By Date of Birth";
            this.radioButtonSearchByDate.UseVisualStyleBackColor = true;
            this.radioButtonSearchByDate.CheckedChanged += new System.EventHandler(this.radioButtonSearchByDate_CheckedChanged);
            // 
            // radioButtonSearchByNameAndDate
            // 
            this.radioButtonSearchByNameAndDate.AutoSize = true;
            this.radioButtonSearchByNameAndDate.Location = new System.Drawing.Point(283, 7);
            this.radioButtonSearchByNameAndDate.Name = "radioButtonSearchByNameAndDate";
            this.radioButtonSearchByNameAndDate.Size = new System.Drawing.Size(151, 17);
            this.radioButtonSearchByNameAndDate.TabIndex = 22;
            this.radioButtonSearchByNameAndDate.TabStop = true;
            this.radioButtonSearchByNameAndDate.Text = "By Name and Date of Birth";
            this.radioButtonSearchByNameAndDate.UseVisualStyleBackColor = true;
            this.radioButtonSearchByNameAndDate.CheckedChanged += new System.EventHandler(this.radioButtonSearchByNameAndDate_CheckedChanged);
            // 
            // radioButtonSearchByName
            // 
            this.radioButtonSearchByName.AutoSize = true;
            this.radioButtonSearchByName.Checked = true;
            this.radioButtonSearchByName.Location = new System.Drawing.Point(207, 7);
            this.radioButtonSearchByName.Name = "radioButtonSearchByName";
            this.radioButtonSearchByName.Size = new System.Drawing.Size(68, 17);
            this.radioButtonSearchByName.TabIndex = 21;
            this.radioButtonSearchByName.TabStop = true;
            this.radioButtonSearchByName.Text = "By Name";
            this.radioButtonSearchByName.UseVisualStyleBackColor = true;
            this.radioButtonSearchByName.CheckedChanged += new System.EventHandler(this.radioButtonSearchByName_CheckedChanged);
            // 
            // labelFullName
            // 
            this.labelFullName.AutoSize = true;
            this.labelFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelFullName.Location = new System.Drawing.Point(126, 47);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(75, 17);
            this.labelFullName.TabIndex = 20;
            this.labelFullName.Text = "Full Name:";
            // 
            // textBoxSearchFullName
            // 
            this.textBoxSearchFullName.Location = new System.Drawing.Point(207, 47);
            this.textBoxSearchFullName.Name = "textBoxSearchFullName";
            this.textBoxSearchFullName.Size = new System.Drawing.Size(162, 20);
            this.textBoxSearchFullName.TabIndex = 19;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonSearch.Location = new System.Drawing.Point(8, 44);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 18;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // labelDatePickerSearchDOB
            // 
            this.labelDatePickerSearchDOB.AutoSize = true;
            this.labelDatePickerSearchDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelDatePickerSearchDOB.Location = new System.Drawing.Point(385, 48);
            this.labelDatePickerSearchDOB.Name = "labelDatePickerSearchDOB";
            this.labelDatePickerSearchDOB.Size = new System.Drawing.Size(91, 17);
            this.labelDatePickerSearchDOB.TabIndex = 17;
            this.labelDatePickerSearchDOB.Text = "Date of Birth:";
            this.labelDatePickerSearchDOB.Visible = false;
            // 
            // dateTimePickerSearch
            // 
            this.dateTimePickerSearch.Location = new System.Drawing.Point(482, 47);
            this.dateTimePickerSearch.Name = "dateTimePickerSearch";
            this.dateTimePickerSearch.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerSearch.TabIndex = 16;
            this.dateTimePickerSearch.Visible = false;
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelSearch.Location = new System.Drawing.Point(8, 7);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(143, 18);
            this.labelSearch.TabIndex = 15;
            this.labelSearch.Text = "Search For a Patient";
            // 
            // dataGridViewSearch
            // 
            this.dataGridViewSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSearch.Location = new System.Drawing.Point(8, 75);
            this.dataGridViewSearch.Name = "dataGridViewSearch";
            this.dataGridViewSearch.ReadOnly = true;
            this.dataGridViewSearch.Size = new System.Drawing.Size(674, 237);
            this.dataGridViewSearch.TabIndex = 0;
            // 
            // tabPageNewAppointment
            // 
            this.tabPageNewAppointment.Controls.Add(this.dateTimePickerNewAppointmentTime);
            this.tabPageNewAppointment.Controls.Add(this.labelNewAppointmentTime);
            this.tabPageNewAppointment.Controls.Add(this.labelNewAppointment);
            this.tabPageNewAppointment.Controls.Add(this.labelScheduleAppointmentStatus);
            this.tabPageNewAppointment.Controls.Add(this.dateTimePickerNewAppointmentDate);
            this.tabPageNewAppointment.Controls.Add(this.buttonScheduleAppointment);
            this.tabPageNewAppointment.Controls.Add(this.richTextBoxAppointmentPurpose);
            this.tabPageNewAppointment.Controls.Add(this.textBoxDoctorID);
            this.tabPageNewAppointment.Controls.Add(this.textBoxPatientIDNewAppointment);
            this.tabPageNewAppointment.Controls.Add(this.labelAppointmentPurpose);
            this.tabPageNewAppointment.Controls.Add(this.labelDoctorIDAppointment);
            this.tabPageNewAppointment.Controls.Add(this.labelNewAppointmentDate);
            this.tabPageNewAppointment.Controls.Add(this.labelPatientIDNewAppointment);
            this.tabPageNewAppointment.Location = new System.Drawing.Point(4, 22);
            this.tabPageNewAppointment.Name = "tabPageNewAppointment";
            this.tabPageNewAppointment.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNewAppointment.Size = new System.Drawing.Size(689, 329);
            this.tabPageNewAppointment.TabIndex = 3;
            this.tabPageNewAppointment.Text = "New Appointment";
            this.tabPageNewAppointment.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerNewAppointmentTime
            // 
            this.dateTimePickerNewAppointmentTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerNewAppointmentTime.Location = new System.Drawing.Point(391, 78);
            this.dateTimePickerNewAppointmentTime.Name = "dateTimePickerNewAppointmentTime";
            this.dateTimePickerNewAppointmentTime.ShowUpDown = true;
            this.dateTimePickerNewAppointmentTime.Size = new System.Drawing.Size(102, 20);
            this.dateTimePickerNewAppointmentTime.TabIndex = 13;
            // 
            // labelNewAppointmentTime
            // 
            this.labelNewAppointmentTime.AutoSize = true;
            this.labelNewAppointmentTime.Location = new System.Drawing.Point(290, 84);
            this.labelNewAppointmentTime.Name = "labelNewAppointmentTime";
            this.labelNewAppointmentTime.Size = new System.Drawing.Size(95, 13);
            this.labelNewAppointmentTime.TabIndex = 12;
            this.labelNewAppointmentTime.Text = "Appointment Time:";
            // 
            // labelNewAppointment
            // 
            this.labelNewAppointment.AutoSize = true;
            this.labelNewAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelNewAppointment.Location = new System.Drawing.Point(7, 7);
            this.labelNewAppointment.Name = "labelNewAppointment";
            this.labelNewAppointment.Size = new System.Drawing.Size(201, 18);
            this.labelNewAppointment.TabIndex = 11;
            this.labelNewAppointment.Text = "Schedule a New Appointment";
            // 
            // labelScheduleAppointmentStatus
            // 
            this.labelScheduleAppointmentStatus.AutoSize = true;
            this.labelScheduleAppointmentStatus.ForeColor = System.Drawing.Color.Black;
            this.labelScheduleAppointmentStatus.Location = new System.Drawing.Point(309, 289);
            this.labelScheduleAppointmentStatus.Name = "labelScheduleAppointmentStatus";
            this.labelScheduleAppointmentStatus.Size = new System.Drawing.Size(37, 13);
            this.labelScheduleAppointmentStatus.TabIndex = 10;
            this.labelScheduleAppointmentStatus.Text = "Status";
            this.labelScheduleAppointmentStatus.Visible = false;
            // 
            // dateTimePickerNewAppointmentDate
            // 
            this.dateTimePickerNewAppointmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerNewAppointmentDate.Location = new System.Drawing.Point(166, 78);
            this.dateTimePickerNewAppointmentDate.Name = "dateTimePickerNewAppointmentDate";
            this.dateTimePickerNewAppointmentDate.Size = new System.Drawing.Size(102, 20);
            this.dateTimePickerNewAppointmentDate.TabIndex = 9;
            // 
            // buttonScheduleAppointment
            // 
            this.buttonScheduleAppointment.Location = new System.Drawing.Point(271, 246);
            this.buttonScheduleAppointment.Name = "buttonScheduleAppointment";
            this.buttonScheduleAppointment.Size = new System.Drawing.Size(75, 23);
            this.buttonScheduleAppointment.TabIndex = 8;
            this.buttonScheduleAppointment.Text = "Schedule";
            this.buttonScheduleAppointment.UseVisualStyleBackColor = true;
            this.buttonScheduleAppointment.Click += new System.EventHandler(this.buttonScheduleAppointment_Click);
            // 
            // richTextBoxAppointmentPurpose
            // 
            this.richTextBoxAppointmentPurpose.Location = new System.Drawing.Point(166, 131);
            this.richTextBoxAppointmentPurpose.Name = "richTextBoxAppointmentPurpose";
            this.richTextBoxAppointmentPurpose.Size = new System.Drawing.Size(180, 96);
            this.richTextBoxAppointmentPurpose.TabIndex = 7;
            this.richTextBoxAppointmentPurpose.Text = "";
            // 
            // textBoxDoctorID
            // 
            this.textBoxDoctorID.Location = new System.Drawing.Point(166, 104);
            this.textBoxDoctorID.Name = "textBoxDoctorID";
            this.textBoxDoctorID.Size = new System.Drawing.Size(100, 20);
            this.textBoxDoctorID.TabIndex = 6;
            // 
            // textBoxPatientIDNewAppointment
            // 
            this.textBoxPatientIDNewAppointment.Location = new System.Drawing.Point(166, 52);
            this.textBoxPatientIDNewAppointment.Name = "textBoxPatientIDNewAppointment";
            this.textBoxPatientIDNewAppointment.Size = new System.Drawing.Size(100, 20);
            this.textBoxPatientIDNewAppointment.TabIndex = 4;
            // 
            // labelAppointmentPurpose
            // 
            this.labelAppointmentPurpose.AutoSize = true;
            this.labelAppointmentPurpose.Location = new System.Drawing.Point(96, 134);
            this.labelAppointmentPurpose.Name = "labelAppointmentPurpose";
            this.labelAppointmentPurpose.Size = new System.Drawing.Size(49, 13);
            this.labelAppointmentPurpose.TabIndex = 3;
            this.labelAppointmentPurpose.Text = "Purpose:";
            // 
            // labelDoctorIDAppointment
            // 
            this.labelDoctorIDAppointment.AutoSize = true;
            this.labelDoctorIDAppointment.Location = new System.Drawing.Point(89, 107);
            this.labelDoctorIDAppointment.Name = "labelDoctorIDAppointment";
            this.labelDoctorIDAppointment.Size = new System.Drawing.Size(56, 13);
            this.labelDoctorIDAppointment.TabIndex = 2;
            this.labelDoctorIDAppointment.Text = "Doctor ID:";
            // 
            // labelNewAppointmentDate
            // 
            this.labelNewAppointmentDate.AutoSize = true;
            this.labelNewAppointmentDate.Location = new System.Drawing.Point(50, 84);
            this.labelNewAppointmentDate.Name = "labelNewAppointmentDate";
            this.labelNewAppointmentDate.Size = new System.Drawing.Size(95, 13);
            this.labelNewAppointmentDate.TabIndex = 1;
            this.labelNewAppointmentDate.Text = "Appointment Date:";
            // 
            // labelPatientIDNewAppointment
            // 
            this.labelPatientIDNewAppointment.AutoSize = true;
            this.labelPatientIDNewAppointment.Location = new System.Drawing.Point(88, 55);
            this.labelPatientIDNewAppointment.Name = "labelPatientIDNewAppointment";
            this.labelPatientIDNewAppointment.Size = new System.Drawing.Size(57, 13);
            this.labelPatientIDNewAppointment.TabIndex = 0;
            this.labelPatientIDNewAppointment.Text = "Patient ID:";
            // 
            // tabPageAppointment
            // 
            this.tabPageAppointment.Controls.Add(this.radioButtonSearchByApptDate);
            this.tabPageAppointment.Controls.Add(this.radioButtonSearchByIDAndDate);
            this.tabPageAppointment.Controls.Add(this.radioButtonSearchByID);
            this.tabPageAppointment.Controls.Add(this.groupBoxVitalSigns);
            this.tabPageAppointment.Controls.Add(this.dataGridViewAppointment);
            this.tabPageAppointment.Controls.Add(this.labelAppointmentStatus);
            this.tabPageAppointment.Controls.Add(this.buttonSearchAppointment);
            this.tabPageAppointment.Controls.Add(this.textBoxPatientIDAppointment);
            this.tabPageAppointment.Controls.Add(this.dateTimePickerAppointmentTime);
            this.tabPageAppointment.Controls.Add(this.dateTimePickerAppointmentDate);
            this.tabPageAppointment.Controls.Add(this.labelAppointmentTime);
            this.tabPageAppointment.Controls.Add(this.labelAppointmentDate);
            this.tabPageAppointment.Controls.Add(this.labelPatientIDAppointment);
            this.tabPageAppointment.Controls.Add(this.labelRecordAppointment);
            this.tabPageAppointment.Location = new System.Drawing.Point(4, 22);
            this.tabPageAppointment.Name = "tabPageAppointment";
            this.tabPageAppointment.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAppointment.Size = new System.Drawing.Size(689, 329);
            this.tabPageAppointment.TabIndex = 5;
            this.tabPageAppointment.Text = "Appointment";
            this.tabPageAppointment.UseVisualStyleBackColor = true;
            // 
            // radioButtonSearchByApptDate
            // 
            this.radioButtonSearchByApptDate.AutoSize = true;
            this.radioButtonSearchByApptDate.Location = new System.Drawing.Point(269, 39);
            this.radioButtonSearchByApptDate.Name = "radioButtonSearchByApptDate";
            this.radioButtonSearchByApptDate.Size = new System.Drawing.Size(76, 17);
            this.radioButtonSearchByApptDate.TabIndex = 25;
            this.radioButtonSearchByApptDate.Text = "Appt. Date";
            this.radioButtonSearchByApptDate.UseVisualStyleBackColor = true;
            this.radioButtonSearchByApptDate.CheckedChanged += new System.EventHandler(this.radioButtonSearchByApptDate_CheckedChanged);
            // 
            // radioButtonSearchByIDAndDate
            // 
            this.radioButtonSearchByIDAndDate.AutoSize = true;
            this.radioButtonSearchByIDAndDate.Checked = true;
            this.radioButtonSearchByIDAndDate.Location = new System.Drawing.Point(119, 37);
            this.radioButtonSearchByIDAndDate.Name = "radioButtonSearchByIDAndDate";
            this.radioButtonSearchByIDAndDate.Size = new System.Drawing.Size(111, 17);
            this.radioButtonSearchByIDAndDate.TabIndex = 24;
            this.radioButtonSearchByIDAndDate.TabStop = true;
            this.radioButtonSearchByIDAndDate.Text = "ID and Appt. Date";
            this.radioButtonSearchByIDAndDate.UseVisualStyleBackColor = true;
            this.radioButtonSearchByIDAndDate.CheckedChanged += new System.EventHandler(this.radioButtonSearchByIDAndDate_CheckedChanged);
            // 
            // radioButtonSearchByID
            // 
            this.radioButtonSearchByID.AutoSize = true;
            this.radioButtonSearchByID.Location = new System.Drawing.Point(36, 37);
            this.radioButtonSearchByID.Name = "radioButtonSearchByID";
            this.radioButtonSearchByID.Size = new System.Drawing.Size(36, 17);
            this.radioButtonSearchByID.TabIndex = 23;
            this.radioButtonSearchByID.Text = "ID";
            this.radioButtonSearchByID.UseVisualStyleBackColor = true;
            this.radioButtonSearchByID.CheckedChanged += new System.EventHandler(this.radioButtonSearchByID_CheckedChanged);
            // 
            // groupBoxVitalSigns
            // 
            this.groupBoxVitalSigns.Controls.Add(this.buttonFinish);
            this.groupBoxVitalSigns.Controls.Add(this.buttonOrderRecordTest);
            this.groupBoxVitalSigns.Controls.Add(this.buttonDiagnose);
            this.groupBoxVitalSigns.Controls.Add(this.labelUpdateAppointmentStatus);
            this.groupBoxVitalSigns.Controls.Add(this.numericUpDownBPSystolic);
            this.groupBoxVitalSigns.Controls.Add(this.labelBPSystolic);
            this.groupBoxVitalSigns.Controls.Add(this.labelSymptoms);
            this.groupBoxVitalSigns.Controls.Add(this.numericUpDownBPDiastolic);
            this.groupBoxVitalSigns.Controls.Add(this.labelBPDiastolic);
            this.groupBoxVitalSigns.Controls.Add(this.numericUpDownBodyTemp);
            this.groupBoxVitalSigns.Controls.Add(this.buttonRecord);
            this.groupBoxVitalSigns.Controls.Add(this.richTextBoxSymptoms);
            this.groupBoxVitalSigns.Controls.Add(this.labelBodyTemp);
            this.groupBoxVitalSigns.Controls.Add(this.numericUpDownPulse);
            this.groupBoxVitalSigns.Controls.Add(this.labelPulse);
            this.groupBoxVitalSigns.Enabled = false;
            this.groupBoxVitalSigns.Location = new System.Drawing.Point(381, 7);
            this.groupBoxVitalSigns.Name = "groupBoxVitalSigns";
            this.groupBoxVitalSigns.Size = new System.Drawing.Size(301, 313);
            this.groupBoxVitalSigns.TabIndex = 22;
            this.groupBoxVitalSigns.TabStop = false;
            this.groupBoxVitalSigns.Text = "Vital Signs";
            // 
            // buttonFinish
            // 
            this.buttonFinish.Location = new System.Drawing.Point(6, 284);
            this.buttonFinish.Name = "buttonFinish";
            this.buttonFinish.Size = new System.Drawing.Size(75, 23);
            this.buttonFinish.TabIndex = 24;
            this.buttonFinish.Text = "Finish";
            this.buttonFinish.UseVisualStyleBackColor = true;
            this.buttonFinish.Click += new System.EventHandler(this.buttonFinish_Click);
            // 
            // buttonOrderRecordTest
            // 
            this.buttonOrderRecordTest.Location = new System.Drawing.Point(6, 226);
            this.buttonOrderRecordTest.Name = "buttonOrderRecordTest";
            this.buttonOrderRecordTest.Size = new System.Drawing.Size(75, 23);
            this.buttonOrderRecordTest.TabIndex = 23;
            this.buttonOrderRecordTest.Text = "Test";
            this.buttonOrderRecordTest.UseVisualStyleBackColor = true;
            this.buttonOrderRecordTest.Click += new System.EventHandler(this.buttonOrderRecordTest_Click);
            // 
            // buttonDiagnose
            // 
            this.buttonDiagnose.Location = new System.Drawing.Point(6, 255);
            this.buttonDiagnose.Name = "buttonDiagnose";
            this.buttonDiagnose.Size = new System.Drawing.Size(75, 23);
            this.buttonDiagnose.TabIndex = 22;
            this.buttonDiagnose.Text = "Diagnose";
            this.buttonDiagnose.UseVisualStyleBackColor = true;
            this.buttonDiagnose.Click += new System.EventHandler(this.buttonDiagnose_Click);
            // 
            // labelUpdateAppointmentStatus
            // 
            this.labelUpdateAppointmentStatus.AutoSize = true;
            this.labelUpdateAppointmentStatus.Location = new System.Drawing.Point(161, 289);
            this.labelUpdateAppointmentStatus.Name = "labelUpdateAppointmentStatus";
            this.labelUpdateAppointmentStatus.Size = new System.Drawing.Size(48, 13);
            this.labelUpdateAppointmentStatus.TabIndex = 21;
            this.labelUpdateAppointmentStatus.Text = "Success";
            this.labelUpdateAppointmentStatus.Visible = false;
            // 
            // numericUpDownBPSystolic
            // 
            this.numericUpDownBPSystolic.Location = new System.Drawing.Point(229, 30);
            this.numericUpDownBPSystolic.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDownBPSystolic.Name = "numericUpDownBPSystolic";
            this.numericUpDownBPSystolic.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownBPSystolic.TabIndex = 3;
            // 
            // labelBPSystolic
            // 
            this.labelBPSystolic.AutoSize = true;
            this.labelBPSystolic.Location = new System.Drawing.Point(148, 32);
            this.labelBPSystolic.Name = "labelBPSystolic";
            this.labelBPSystolic.Size = new System.Drawing.Size(63, 13);
            this.labelBPSystolic.TabIndex = 2;
            this.labelBPSystolic.Text = "BP Systolic:";
            // 
            // labelSymptoms
            // 
            this.labelSymptoms.AutoSize = true;
            this.labelSymptoms.Location = new System.Drawing.Point(45, 155);
            this.labelSymptoms.Name = "labelSymptoms";
            this.labelSymptoms.Size = new System.Drawing.Size(58, 13);
            this.labelSymptoms.TabIndex = 20;
            this.labelSymptoms.Text = "Symptoms:";
            // 
            // numericUpDownBPDiastolic
            // 
            this.numericUpDownBPDiastolic.Location = new System.Drawing.Point(229, 56);
            this.numericUpDownBPDiastolic.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownBPDiastolic.Name = "numericUpDownBPDiastolic";
            this.numericUpDownBPDiastolic.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownBPDiastolic.TabIndex = 4;
            // 
            // labelBPDiastolic
            // 
            this.labelBPDiastolic.AutoSize = true;
            this.labelBPDiastolic.Location = new System.Drawing.Point(144, 58);
            this.labelBPDiastolic.Name = "labelBPDiastolic";
            this.labelBPDiastolic.Size = new System.Drawing.Size(67, 13);
            this.labelBPDiastolic.TabIndex = 1;
            this.labelBPDiastolic.Text = "BP Diastolic:";
            // 
            // numericUpDownBodyTemp
            // 
            this.numericUpDownBodyTemp.DecimalPlaces = 2;
            this.numericUpDownBodyTemp.Location = new System.Drawing.Point(229, 101);
            this.numericUpDownBodyTemp.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numericUpDownBodyTemp.Name = "numericUpDownBodyTemp";
            this.numericUpDownBodyTemp.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownBodyTemp.TabIndex = 6;
            // 
            // buttonRecord
            // 
            this.buttonRecord.Location = new System.Drawing.Point(216, 284);
            this.buttonRecord.Name = "buttonRecord";
            this.buttonRecord.Size = new System.Drawing.Size(75, 23);
            this.buttonRecord.TabIndex = 16;
            this.buttonRecord.Text = "Record";
            this.buttonRecord.UseVisualStyleBackColor = true;
            this.buttonRecord.Click += new System.EventHandler(this.buttonRecord_Click);
            // 
            // richTextBoxSymptoms
            // 
            this.richTextBoxSymptoms.Location = new System.Drawing.Point(109, 152);
            this.richTextBoxSymptoms.Name = "richTextBoxSymptoms";
            this.richTextBoxSymptoms.Size = new System.Drawing.Size(182, 120);
            this.richTextBoxSymptoms.TabIndex = 18;
            this.richTextBoxSymptoms.Text = "";
            // 
            // labelBodyTemp
            // 
            this.labelBodyTemp.AutoSize = true;
            this.labelBodyTemp.Location = new System.Drawing.Point(114, 103);
            this.labelBodyTemp.Name = "labelBodyTemp";
            this.labelBodyTemp.Size = new System.Drawing.Size(97, 13);
            this.labelBodyTemp.TabIndex = 5;
            this.labelBodyTemp.Text = "Body Temperature:";
            // 
            // numericUpDownPulse
            // 
            this.numericUpDownPulse.Location = new System.Drawing.Point(229, 126);
            this.numericUpDownPulse.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDownPulse.Name = "numericUpDownPulse";
            this.numericUpDownPulse.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownPulse.TabIndex = 8;
            // 
            // labelPulse
            // 
            this.labelPulse.AutoSize = true;
            this.labelPulse.Location = new System.Drawing.Point(173, 128);
            this.labelPulse.Name = "labelPulse";
            this.labelPulse.Size = new System.Drawing.Size(36, 13);
            this.labelPulse.TabIndex = 7;
            this.labelPulse.Text = "Pulse:";
            // 
            // dataGridViewAppointment
            // 
            this.dataGridViewAppointment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAppointment.Location = new System.Drawing.Point(27, 158);
            this.dataGridViewAppointment.MultiSelect = false;
            this.dataGridViewAppointment.Name = "dataGridViewAppointment";
            this.dataGridViewAppointment.ReadOnly = true;
            this.dataGridViewAppointment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAppointment.Size = new System.Drawing.Size(332, 150);
            this.dataGridViewAppointment.TabIndex = 21;
            this.dataGridViewAppointment.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAppointment_CellContentDoubleClick);
            // 
            // labelAppointmentStatus
            // 
            this.labelAppointmentStatus.AutoSize = true;
            this.labelAppointmentStatus.Location = new System.Drawing.Point(24, 135);
            this.labelAppointmentStatus.Name = "labelAppointmentStatus";
            this.labelAppointmentStatus.Size = new System.Drawing.Size(35, 13);
            this.labelAppointmentStatus.TabIndex = 19;
            this.labelAppointmentStatus.Text = "status";
            this.labelAppointmentStatus.Visible = false;
            // 
            // buttonSearchAppointment
            // 
            this.buttonSearchAppointment.Location = new System.Drawing.Point(285, 125);
            this.buttonSearchAppointment.Name = "buttonSearchAppointment";
            this.buttonSearchAppointment.Size = new System.Drawing.Size(75, 23);
            this.buttonSearchAppointment.TabIndex = 17;
            this.buttonSearchAppointment.Text = "Search";
            this.buttonSearchAppointment.UseVisualStyleBackColor = true;
            this.buttonSearchAppointment.Click += new System.EventHandler(this.buttonSearchAppointment_Click);
            // 
            // textBoxPatientIDAppointment
            // 
            this.textBoxPatientIDAppointment.Location = new System.Drawing.Point(87, 73);
            this.textBoxPatientIDAppointment.Name = "textBoxPatientIDAppointment";
            this.textBoxPatientIDAppointment.Size = new System.Drawing.Size(100, 20);
            this.textBoxPatientIDAppointment.TabIndex = 15;
            // 
            // dateTimePickerAppointmentTime
            // 
            this.dateTimePickerAppointmentTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerAppointmentTime.Location = new System.Drawing.Point(269, 99);
            this.dateTimePickerAppointmentTime.Name = "dateTimePickerAppointmentTime";
            this.dateTimePickerAppointmentTime.ShowUpDown = true;
            this.dateTimePickerAppointmentTime.Size = new System.Drawing.Size(91, 20);
            this.dateTimePickerAppointmentTime.TabIndex = 14;
            // 
            // dateTimePickerAppointmentDate
            // 
            this.dateTimePickerAppointmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerAppointmentDate.Location = new System.Drawing.Point(87, 99);
            this.dateTimePickerAppointmentDate.Name = "dateTimePickerAppointmentDate";
            this.dateTimePickerAppointmentDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAppointmentDate.TabIndex = 13;
            // 
            // labelAppointmentTime
            // 
            this.labelAppointmentTime.AutoSize = true;
            this.labelAppointmentTime.Location = new System.Drawing.Point(205, 105);
            this.labelAppointmentTime.Name = "labelAppointmentTime";
            this.labelAppointmentTime.Size = new System.Drawing.Size(58, 13);
            this.labelAppointmentTime.TabIndex = 12;
            this.labelAppointmentTime.Text = "Appt Time:";
            // 
            // labelAppointmentDate
            // 
            this.labelAppointmentDate.AutoSize = true;
            this.labelAppointmentDate.Location = new System.Drawing.Point(23, 105);
            this.labelAppointmentDate.Name = "labelAppointmentDate";
            this.labelAppointmentDate.Size = new System.Drawing.Size(58, 13);
            this.labelAppointmentDate.TabIndex = 11;
            this.labelAppointmentDate.Text = "Appt Date:";
            // 
            // labelPatientIDAppointment
            // 
            this.labelPatientIDAppointment.AutoSize = true;
            this.labelPatientIDAppointment.Location = new System.Drawing.Point(24, 76);
            this.labelPatientIDAppointment.Name = "labelPatientIDAppointment";
            this.labelPatientIDAppointment.Size = new System.Drawing.Size(57, 13);
            this.labelPatientIDAppointment.TabIndex = 10;
            this.labelPatientIDAppointment.Text = "Patient ID:";
            // 
            // labelRecordAppointment
            // 
            this.labelRecordAppointment.AutoSize = true;
            this.labelRecordAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelRecordAppointment.Location = new System.Drawing.Point(9, 7);
            this.labelRecordAppointment.Name = "labelRecordAppointment";
            this.labelRecordAppointment.Size = new System.Drawing.Size(221, 18);
            this.labelRecordAppointment.TabIndex = 9;
            this.labelRecordAppointment.Text = "Record Appointment Information";
            // 
            // tabPageTest
            // 
            this.tabPageTest.Controls.Add(this.groupBoxOrderTest);
            this.tabPageTest.Controls.Add(this.groupBoxRecordTestResults);
            this.tabPageTest.Location = new System.Drawing.Point(4, 22);
            this.tabPageTest.Name = "tabPageTest";
            this.tabPageTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTest.Size = new System.Drawing.Size(689, 329);
            this.tabPageTest.TabIndex = 8;
            this.tabPageTest.Text = "Test";
            this.tabPageTest.UseVisualStyleBackColor = true;
            // 
            // groupBoxOrderTest
            // 
            this.groupBoxOrderTest.Controls.Add(this.labelOrderTestStatus);
            this.groupBoxOrderTest.Controls.Add(this.labelOrderTest);
            this.groupBoxOrderTest.Controls.Add(this.buttonOrderTest);
            this.groupBoxOrderTest.Controls.Add(this.dataGridViewOrderTests);
            this.groupBoxOrderTest.Location = new System.Drawing.Point(8, 6);
            this.groupBoxOrderTest.Name = "groupBoxOrderTest";
            this.groupBoxOrderTest.Size = new System.Drawing.Size(230, 315);
            this.groupBoxOrderTest.TabIndex = 4;
            this.groupBoxOrderTest.TabStop = false;
            this.groupBoxOrderTest.Text = "Order Test";
            // 
            // labelOrderTestStatus
            // 
            this.labelOrderTestStatus.AutoSize = true;
            this.labelOrderTestStatus.Location = new System.Drawing.Point(6, 253);
            this.labelOrderTestStatus.Name = "labelOrderTestStatus";
            this.labelOrderTestStatus.Size = new System.Drawing.Size(37, 13);
            this.labelOrderTestStatus.TabIndex = 3;
            this.labelOrderTestStatus.Text = "Status";
            this.labelOrderTestStatus.Visible = false;
            // 
            // labelOrderTest
            // 
            this.labelOrderTest.AutoSize = true;
            this.labelOrderTest.Location = new System.Drawing.Point(7, 43);
            this.labelOrderTest.Name = "labelOrderTest";
            this.labelOrderTest.Size = new System.Drawing.Size(125, 13);
            this.labelOrderTest.TabIndex = 2;
            this.labelOrderTest.Text = "Select the Tests to Order";
            // 
            // buttonOrderTest
            // 
            this.buttonOrderTest.Location = new System.Drawing.Point(141, 248);
            this.buttonOrderTest.Name = "buttonOrderTest";
            this.buttonOrderTest.Size = new System.Drawing.Size(75, 23);
            this.buttonOrderTest.TabIndex = 1;
            this.buttonOrderTest.Text = "Order";
            this.buttonOrderTest.UseVisualStyleBackColor = true;
            this.buttonOrderTest.Click += new System.EventHandler(this.buttonOrderTest_Click);
            // 
            // dataGridViewOrderTests
            // 
            this.dataGridViewOrderTests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewOrderTests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrderTests.Location = new System.Drawing.Point(6, 62);
            this.dataGridViewOrderTests.Name = "dataGridViewOrderTests";
            this.dataGridViewOrderTests.ReadOnly = true;
            this.dataGridViewOrderTests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOrderTests.Size = new System.Drawing.Size(210, 179);
            this.dataGridViewOrderTests.TabIndex = 0;
            // 
            // groupBoxRecordTestResults
            // 
            this.groupBoxRecordTestResults.Controls.Add(this.buttonDoneTest);
            this.groupBoxRecordTestResults.Controls.Add(this.labelTestName);
            this.groupBoxRecordTestResults.Controls.Add(this.labelRecordLabResultsStatus);
            this.groupBoxRecordTestResults.Controls.Add(this.buttonRecordTestResults);
            this.groupBoxRecordTestResults.Controls.Add(this.labelTestResults);
            this.groupBoxRecordTestResults.Controls.Add(this.labelSelectTest);
            this.groupBoxRecordTestResults.Controls.Add(this.dataGridViewLabTest);
            this.groupBoxRecordTestResults.Controls.Add(this.richTextBoxTestResults);
            this.groupBoxRecordTestResults.Location = new System.Drawing.Point(244, 6);
            this.groupBoxRecordTestResults.Name = "groupBoxRecordTestResults";
            this.groupBoxRecordTestResults.Size = new System.Drawing.Size(439, 315);
            this.groupBoxRecordTestResults.TabIndex = 3;
            this.groupBoxRecordTestResults.TabStop = false;
            this.groupBoxRecordTestResults.Text = "Record Test Results";
            // 
            // buttonDoneTest
            // 
            this.buttonDoneTest.Location = new System.Drawing.Point(353, 286);
            this.buttonDoneTest.Name = "buttonDoneTest";
            this.buttonDoneTest.Size = new System.Drawing.Size(75, 23);
            this.buttonDoneTest.TabIndex = 7;
            this.buttonDoneTest.Text = "Done";
            this.buttonDoneTest.UseVisualStyleBackColor = true;
            this.buttonDoneTest.Click += new System.EventHandler(this.buttonDoneTest_Click);
            // 
            // labelTestName
            // 
            this.labelTestName.AutoSize = true;
            this.labelTestName.Location = new System.Drawing.Point(266, 43);
            this.labelTestName.Name = "labelTestName";
            this.labelTestName.Size = new System.Drawing.Size(35, 13);
            this.labelTestName.TabIndex = 6;
            this.labelTestName.Text = "Name";
            this.labelTestName.Visible = false;
            // 
            // labelRecordLabResultsStatus
            // 
            this.labelRecordLabResultsStatus.AutoSize = true;
            this.labelRecordLabResultsStatus.Location = new System.Drawing.Point(215, 253);
            this.labelRecordLabResultsStatus.Name = "labelRecordLabResultsStatus";
            this.labelRecordLabResultsStatus.Size = new System.Drawing.Size(37, 13);
            this.labelRecordLabResultsStatus.TabIndex = 5;
            this.labelRecordLabResultsStatus.Text = "Status";
            this.labelRecordLabResultsStatus.Visible = false;
            // 
            // buttonRecordTestResults
            // 
            this.buttonRecordTestResults.Location = new System.Drawing.Point(353, 247);
            this.buttonRecordTestResults.Name = "buttonRecordTestResults";
            this.buttonRecordTestResults.Size = new System.Drawing.Size(75, 23);
            this.buttonRecordTestResults.TabIndex = 4;
            this.buttonRecordTestResults.Text = "Record";
            this.buttonRecordTestResults.UseVisualStyleBackColor = true;
            this.buttonRecordTestResults.Click += new System.EventHandler(this.buttonRecordTestResults_Click_1);
            // 
            // labelTestResults
            // 
            this.labelTestResults.AutoSize = true;
            this.labelTestResults.Location = new System.Drawing.Point(215, 43);
            this.labelTestResults.Name = "labelTestResults";
            this.labelTestResults.Size = new System.Drawing.Size(45, 13);
            this.labelTestResults.TabIndex = 3;
            this.labelTestResults.Text = "Results:";
            // 
            // labelSelectTest
            // 
            this.labelSelectTest.AutoSize = true;
            this.labelSelectTest.Location = new System.Drawing.Point(7, 43);
            this.labelSelectTest.Name = "labelSelectTest";
            this.labelSelectTest.Size = new System.Drawing.Size(108, 13);
            this.labelSelectTest.TabIndex = 2;
            this.labelSelectTest.Text = "Click to Select a Test";
            // 
            // dataGridViewLabTest
            // 
            this.dataGridViewLabTest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewLabTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLabTest.Location = new System.Drawing.Point(6, 62);
            this.dataGridViewLabTest.Name = "dataGridViewLabTest";
            this.dataGridViewLabTest.ReadOnly = true;
            this.dataGridViewLabTest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLabTest.Size = new System.Drawing.Size(203, 179);
            this.dataGridViewLabTest.TabIndex = 0;
            this.dataGridViewLabTest.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLabTest_CellContentClick);
            // 
            // richTextBoxTestResults
            // 
            this.richTextBoxTestResults.Location = new System.Drawing.Point(215, 62);
            this.richTextBoxTestResults.Name = "richTextBoxTestResults";
            this.richTextBoxTestResults.Size = new System.Drawing.Size(213, 179);
            this.richTextBoxTestResults.TabIndex = 1;
            this.richTextBoxTestResults.Text = "";
            // 
            // tabPageDiagnose
            // 
            this.tabPageDiagnose.Controls.Add(this.groupBoxDiagnosis);
            this.tabPageDiagnose.Location = new System.Drawing.Point(4, 22);
            this.tabPageDiagnose.Name = "tabPageDiagnose";
            this.tabPageDiagnose.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDiagnose.Size = new System.Drawing.Size(689, 329);
            this.tabPageDiagnose.TabIndex = 7;
            this.tabPageDiagnose.Text = "Diagnose";
            this.tabPageDiagnose.UseVisualStyleBackColor = true;
            // 
            // groupBoxDiagnosis
            // 
            this.groupBoxDiagnosis.Controls.Add(this.labelDiagnosisStatus);
            this.groupBoxDiagnosis.Controls.Add(this.buttonCancelDiagnosis);
            this.groupBoxDiagnosis.Controls.Add(this.buttonRecordDiagnosis);
            this.groupBoxDiagnosis.Controls.Add(this.richTextBoxDiagnosis);
            this.groupBoxDiagnosis.Controls.Add(this.radioButtonFinalDiagnosis);
            this.groupBoxDiagnosis.Controls.Add(this.radioButtonInitialDiagnosis);
            this.groupBoxDiagnosis.Location = new System.Drawing.Point(113, 6);
            this.groupBoxDiagnosis.Name = "groupBoxDiagnosis";
            this.groupBoxDiagnosis.Size = new System.Drawing.Size(272, 310);
            this.groupBoxDiagnosis.TabIndex = 2;
            this.groupBoxDiagnosis.TabStop = false;
            this.groupBoxDiagnosis.Text = "Diagnose";
            // 
            // labelDiagnosisStatus
            // 
            this.labelDiagnosisStatus.AutoSize = true;
            this.labelDiagnosisStatus.Location = new System.Drawing.Point(115, 283);
            this.labelDiagnosisStatus.Name = "labelDiagnosisStatus";
            this.labelDiagnosisStatus.Size = new System.Drawing.Size(138, 13);
            this.labelDiagnosisStatus.TabIndex = 8;
            this.labelDiagnosisStatus.Text = "Initial Diagnosis Successful!";
            this.labelDiagnosisStatus.Visible = false;
            // 
            // buttonCancelDiagnosis
            // 
            this.buttonCancelDiagnosis.Location = new System.Drawing.Point(83, 248);
            this.buttonCancelDiagnosis.Name = "buttonCancelDiagnosis";
            this.buttonCancelDiagnosis.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelDiagnosis.TabIndex = 7;
            this.buttonCancelDiagnosis.Text = "Cancel";
            this.buttonCancelDiagnosis.UseVisualStyleBackColor = true;
            this.buttonCancelDiagnosis.Click += new System.EventHandler(this.buttonCancelDiagnosis_Click);
            // 
            // buttonRecordDiagnosis
            // 
            this.buttonRecordDiagnosis.Location = new System.Drawing.Point(177, 248);
            this.buttonRecordDiagnosis.Name = "buttonRecordDiagnosis";
            this.buttonRecordDiagnosis.Size = new System.Drawing.Size(75, 23);
            this.buttonRecordDiagnosis.TabIndex = 6;
            this.buttonRecordDiagnosis.Text = "Record";
            this.buttonRecordDiagnosis.UseVisualStyleBackColor = true;
            this.buttonRecordDiagnosis.Click += new System.EventHandler(this.buttonRecordDiagnosis_Click);
            // 
            // richTextBoxDiagnosis
            // 
            this.richTextBoxDiagnosis.Location = new System.Drawing.Point(6, 79);
            this.richTextBoxDiagnosis.Name = "richTextBoxDiagnosis";
            this.richTextBoxDiagnosis.Size = new System.Drawing.Size(260, 163);
            this.richTextBoxDiagnosis.TabIndex = 3;
            this.richTextBoxDiagnosis.Text = "";
            // 
            // radioButtonFinalDiagnosis
            // 
            this.radioButtonFinalDiagnosis.AutoSize = true;
            this.radioButtonFinalDiagnosis.Location = new System.Drawing.Point(206, 55);
            this.radioButtonFinalDiagnosis.Name = "radioButtonFinalDiagnosis";
            this.radioButtonFinalDiagnosis.Size = new System.Drawing.Size(47, 17);
            this.radioButtonFinalDiagnosis.TabIndex = 5;
            this.radioButtonFinalDiagnosis.Text = "Final";
            this.radioButtonFinalDiagnosis.UseVisualStyleBackColor = true;
            // 
            // radioButtonInitialDiagnosis
            // 
            this.radioButtonInitialDiagnosis.AutoSize = true;
            this.radioButtonInitialDiagnosis.Checked = true;
            this.radioButtonInitialDiagnosis.Location = new System.Drawing.Point(147, 55);
            this.radioButtonInitialDiagnosis.Name = "radioButtonInitialDiagnosis";
            this.radioButtonInitialDiagnosis.Size = new System.Drawing.Size(49, 17);
            this.radioButtonInitialDiagnosis.TabIndex = 4;
            this.radioButtonInitialDiagnosis.TabStop = true;
            this.radioButtonInitialDiagnosis.Text = "Initial";
            this.radioButtonInitialDiagnosis.UseVisualStyleBackColor = true;
            // 
            // tabPageAdmin
            // 
            this.tabPageAdmin.Controls.Add(this.dataGridViewAdmin);
            this.tabPageAdmin.Controls.Add(this.labelQueryBox);
            this.tabPageAdmin.Controls.Add(this.buttonGoPersonsSQL);
            this.tabPageAdmin.Controls.Add(this.labelAdmin);
            this.tabPageAdmin.Controls.Add(this.richTextBoxAdminSQL);
            this.tabPageAdmin.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdmin.Name = "tabPageAdmin";
            this.tabPageAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdmin.Size = new System.Drawing.Size(689, 329);
            this.tabPageAdmin.TabIndex = 6;
            this.tabPageAdmin.Text = "Admin";
            this.tabPageAdmin.UseVisualStyleBackColor = true;
            // 
            // dataGridViewAdmin
            // 
            this.dataGridViewAdmin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdmin.Location = new System.Drawing.Point(11, 127);
            this.dataGridViewAdmin.Name = "dataGridViewAdmin";
            this.dataGridViewAdmin.ReadOnly = true;
            this.dataGridViewAdmin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAdmin.Size = new System.Drawing.Size(590, 194);
            this.dataGridViewAdmin.TabIndex = 4;
            // 
            // labelQueryBox
            // 
            this.labelQueryBox.AutoSize = true;
            this.labelQueryBox.Location = new System.Drawing.Point(9, 40);
            this.labelQueryBox.Name = "labelQueryBox";
            this.labelQueryBox.Size = new System.Drawing.Size(56, 13);
            this.labelQueryBox.TabIndex = 3;
            this.labelQueryBox.Text = "Query Box";
            // 
            // buttonGoPersonsSQL
            // 
            this.buttonGoPersonsSQL.Location = new System.Drawing.Point(607, 56);
            this.buttonGoPersonsSQL.Name = "buttonGoPersonsSQL";
            this.buttonGoPersonsSQL.Size = new System.Drawing.Size(76, 24);
            this.buttonGoPersonsSQL.TabIndex = 2;
            this.buttonGoPersonsSQL.Text = "Go";
            this.buttonGoPersonsSQL.UseVisualStyleBackColor = true;
            this.buttonGoPersonsSQL.Click += new System.EventHandler(this.buttonGoPersonsSQL_Click);
            // 
            // labelAdmin
            // 
            this.labelAdmin.AutoSize = true;
            this.labelAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelAdmin.Location = new System.Drawing.Point(8, 7);
            this.labelAdmin.Name = "labelAdmin";
            this.labelAdmin.Size = new System.Drawing.Size(186, 18);
            this.labelAdmin.TabIndex = 1;
            this.labelAdmin.Text = "Admin Access to Database";
            // 
            // richTextBoxAdminSQL
            // 
            this.richTextBoxAdminSQL.Location = new System.Drawing.Point(8, 56);
            this.richTextBoxAdminSQL.Name = "richTextBoxAdminSQL";
            this.richTextBoxAdminSQL.Size = new System.Drawing.Size(593, 53);
            this.richTextBoxAdminSQL.TabIndex = 0;
            this.richTextBoxAdminSQL.Text = "";
            // 
            // tabPageRegisterNurse
            // 
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNursePassword);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNursePassword);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseStatus);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseState);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseZipCode);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNursePhoneNumber);
            this.tabPageRegisterNurse.Controls.Add(this.label9);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseAddress);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseDateOfBirth);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseCity);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseFirstName);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseLastName);
            this.tabPageRegisterNurse.Controls.Add(this.comboBoxRegisterNurseState);
            this.tabPageRegisterNurse.Controls.Add(this.buttonRegisterNurse);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNursePhoneNumberError);
            this.tabPageRegisterNurse.Controls.Add(this.labelRegisterNurseZipCodeError);
            this.tabPageRegisterNurse.Controls.Add(this.dateTimePickerRegisterNurse);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNurseLastName);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNurseFirstName);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNurseAddress);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNurseCity);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNursePhoneNumber);
            this.tabPageRegisterNurse.Controls.Add(this.textBoxRegisterNurseZipCode);
            this.tabPageRegisterNurse.Controls.Add(this.registerNurseLabel);
            this.tabPageRegisterNurse.Location = new System.Drawing.Point(4, 22);
            this.tabPageRegisterNurse.Name = "tabPageRegisterNurse";
            this.tabPageRegisterNurse.Size = new System.Drawing.Size(689, 329);
            this.tabPageRegisterNurse.TabIndex = 9;
            this.tabPageRegisterNurse.Text = "Register Nurse";
            this.tabPageRegisterNurse.UseVisualStyleBackColor = true;
            // 
            // labelRegisterNursePassword
            // 
            this.labelRegisterNursePassword.AutoSize = true;
            this.labelRegisterNursePassword.Location = new System.Drawing.Point(80, 271);
            this.labelRegisterNursePassword.Name = "labelRegisterNursePassword";
            this.labelRegisterNursePassword.Size = new System.Drawing.Size(56, 13);
            this.labelRegisterNursePassword.TabIndex = 40;
            this.labelRegisterNursePassword.Text = "Password:";
            // 
            // textBoxRegisterNursePassword
            // 
            this.textBoxRegisterNursePassword.Location = new System.Drawing.Point(145, 268);
            this.textBoxRegisterNursePassword.Name = "textBoxRegisterNursePassword";
            this.textBoxRegisterNursePassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNursePassword.TabIndex = 39;
            // 
            // labelRegisterNurseStatus
            // 
            this.labelRegisterNurseStatus.AutoSize = true;
            this.labelRegisterNurseStatus.Location = new System.Drawing.Point(166, 291);
            this.labelRegisterNurseStatus.Name = "labelRegisterNurseStatus";
            this.labelRegisterNurseStatus.Size = new System.Drawing.Size(79, 13);
            this.labelRegisterNurseStatus.TabIndex = 38;
            this.labelRegisterNurseStatus.Text = "Register Status";
            this.labelRegisterNurseStatus.Visible = false;
            // 
            // labelRegisterNurseState
            // 
            this.labelRegisterNurseState.AutoSize = true;
            this.labelRegisterNurseState.Location = new System.Drawing.Point(312, 191);
            this.labelRegisterNurseState.Name = "labelRegisterNurseState";
            this.labelRegisterNurseState.Size = new System.Drawing.Size(35, 13);
            this.labelRegisterNurseState.TabIndex = 37;
            this.labelRegisterNurseState.Text = "State:";
            // 
            // labelRegisterNurseZipCode
            // 
            this.labelRegisterNurseZipCode.AutoSize = true;
            this.labelRegisterNurseZipCode.Location = new System.Drawing.Point(86, 219);
            this.labelRegisterNurseZipCode.Name = "labelRegisterNurseZipCode";
            this.labelRegisterNurseZipCode.Size = new System.Drawing.Size(53, 13);
            this.labelRegisterNurseZipCode.TabIndex = 36;
            this.labelRegisterNurseZipCode.Text = "Zip Code:";
            // 
            // labelRegisterNursePhoneNumber
            // 
            this.labelRegisterNursePhoneNumber.AutoSize = true;
            this.labelRegisterNursePhoneNumber.Location = new System.Drawing.Point(58, 243);
            this.labelRegisterNursePhoneNumber.Name = "labelRegisterNursePhoneNumber";
            this.labelRegisterNursePhoneNumber.Size = new System.Drawing.Size(81, 13);
            this.labelRegisterNursePhoneNumber.TabIndex = 35;
            this.labelRegisterNursePhoneNumber.Text = "Phone Number:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(80, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 34;
            // 
            // labelRegisterNurseAddress
            // 
            this.labelRegisterNurseAddress.AutoSize = true;
            this.labelRegisterNurseAddress.Location = new System.Drawing.Point(95, 165);
            this.labelRegisterNurseAddress.Name = "labelRegisterNurseAddress";
            this.labelRegisterNurseAddress.Size = new System.Drawing.Size(48, 13);
            this.labelRegisterNurseAddress.TabIndex = 33;
            this.labelRegisterNurseAddress.Text = "Address:";
            // 
            // labelRegisterNurseDateOfBirth
            // 
            this.labelRegisterNurseDateOfBirth.AutoSize = true;
            this.labelRegisterNurseDateOfBirth.Location = new System.Drawing.Point(75, 139);
            this.labelRegisterNurseDateOfBirth.Name = "labelRegisterNurseDateOfBirth";
            this.labelRegisterNurseDateOfBirth.Size = new System.Drawing.Size(69, 13);
            this.labelRegisterNurseDateOfBirth.TabIndex = 32;
            this.labelRegisterNurseDateOfBirth.Text = "Date of Birth:";
            // 
            // labelRegisterNurseCity
            // 
            this.labelRegisterNurseCity.AutoSize = true;
            this.labelRegisterNurseCity.Location = new System.Drawing.Point(113, 195);
            this.labelRegisterNurseCity.Name = "labelRegisterNurseCity";
            this.labelRegisterNurseCity.Size = new System.Drawing.Size(27, 13);
            this.labelRegisterNurseCity.TabIndex = 31;
            this.labelRegisterNurseCity.Text = "City:";
            // 
            // labelRegisterNurseFirstName
            // 
            this.labelRegisterNurseFirstName.AutoSize = true;
            this.labelRegisterNurseFirstName.Location = new System.Drawing.Point(80, 79);
            this.labelRegisterNurseFirstName.Name = "labelRegisterNurseFirstName";
            this.labelRegisterNurseFirstName.Size = new System.Drawing.Size(60, 13);
            this.labelRegisterNurseFirstName.TabIndex = 30;
            this.labelRegisterNurseFirstName.Text = "First Name:";
            // 
            // labelRegisterNurseLastName
            // 
            this.labelRegisterNurseLastName.AutoSize = true;
            this.labelRegisterNurseLastName.Location = new System.Drawing.Point(80, 107);
            this.labelRegisterNurseLastName.Name = "labelRegisterNurseLastName";
            this.labelRegisterNurseLastName.Size = new System.Drawing.Size(61, 13);
            this.labelRegisterNurseLastName.TabIndex = 29;
            this.labelRegisterNurseLastName.Text = "Last Name:";
            // 
            // comboBoxRegisterNurseState
            // 
            this.comboBoxRegisterNurseState.FormattingEnabled = true;
            this.comboBoxRegisterNurseState.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"});
            this.comboBoxRegisterNurseState.Location = new System.Drawing.Point(356, 187);
            this.comboBoxRegisterNurseState.Name = "comboBoxRegisterNurseState";
            this.comboBoxRegisterNurseState.Size = new System.Drawing.Size(96, 21);
            this.comboBoxRegisterNurseState.TabIndex = 28;
            // 
            // buttonRegisterNurse
            // 
            this.buttonRegisterNurse.Location = new System.Drawing.Point(315, 286);
            this.buttonRegisterNurse.Name = "buttonRegisterNurse";
            this.buttonRegisterNurse.Size = new System.Drawing.Size(75, 23);
            this.buttonRegisterNurse.TabIndex = 27;
            this.buttonRegisterNurse.Text = "Register";
            this.buttonRegisterNurse.UseVisualStyleBackColor = true;
            this.buttonRegisterNurse.Click += new System.EventHandler(this.buttonRegisterNurse_Click);
            // 
            // labelRegisterNursePhoneNumberError
            // 
            this.labelRegisterNursePhoneNumberError.AutoSize = true;
            this.labelRegisterNursePhoneNumberError.ForeColor = System.Drawing.Color.Red;
            this.labelRegisterNursePhoneNumberError.Location = new System.Drawing.Point(276, 243);
            this.labelRegisterNursePhoneNumberError.Name = "labelRegisterNursePhoneNumberError";
            this.labelRegisterNursePhoneNumberError.Size = new System.Drawing.Size(138, 13);
            this.labelRegisterNursePhoneNumberError.TabIndex = 26;
            this.labelRegisterNursePhoneNumberError.Text = "Must Contain Only Numbers";
            this.labelRegisterNursePhoneNumberError.Visible = false;
            // 
            // labelRegisterNurseZipCodeError
            // 
            this.labelRegisterNurseZipCodeError.AutoSize = true;
            this.labelRegisterNurseZipCodeError.ForeColor = System.Drawing.Color.Red;
            this.labelRegisterNurseZipCodeError.Location = new System.Drawing.Point(276, 219);
            this.labelRegisterNurseZipCodeError.Name = "labelRegisterNurseZipCodeError";
            this.labelRegisterNurseZipCodeError.Size = new System.Drawing.Size(138, 13);
            this.labelRegisterNurseZipCodeError.TabIndex = 25;
            this.labelRegisterNurseZipCodeError.Text = "Must Contain Only Numbers";
            this.labelRegisterNurseZipCodeError.Visible = false;
            // 
            // dateTimePickerRegisterNurse
            // 
            this.dateTimePickerRegisterNurse.Location = new System.Drawing.Point(147, 132);
            this.dateTimePickerRegisterNurse.Name = "dateTimePickerRegisterNurse";
            this.dateTimePickerRegisterNurse.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerRegisterNurse.TabIndex = 22;
            // 
            // textBoxRegisterNurseLastName
            // 
            this.textBoxRegisterNurseLastName.Location = new System.Drawing.Point(146, 104);
            this.textBoxRegisterNurseLastName.Name = "textBoxRegisterNurseLastName";
            this.textBoxRegisterNurseLastName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNurseLastName.TabIndex = 21;
            // 
            // textBoxRegisterNurseFirstName
            // 
            this.textBoxRegisterNurseFirstName.Location = new System.Drawing.Point(146, 76);
            this.textBoxRegisterNurseFirstName.Name = "textBoxRegisterNurseFirstName";
            this.textBoxRegisterNurseFirstName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNurseFirstName.TabIndex = 20;
            // 
            // textBoxRegisterNurseAddress
            // 
            this.textBoxRegisterNurseAddress.Location = new System.Drawing.Point(146, 158);
            this.textBoxRegisterNurseAddress.Name = "textBoxRegisterNurseAddress";
            this.textBoxRegisterNurseAddress.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNurseAddress.TabIndex = 19;
            // 
            // textBoxRegisterNurseCity
            // 
            this.textBoxRegisterNurseCity.Location = new System.Drawing.Point(146, 188);
            this.textBoxRegisterNurseCity.Name = "textBoxRegisterNurseCity";
            this.textBoxRegisterNurseCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNurseCity.TabIndex = 18;
            // 
            // textBoxRegisterNursePhoneNumber
            // 
            this.textBoxRegisterNursePhoneNumber.Location = new System.Drawing.Point(145, 240);
            this.textBoxRegisterNursePhoneNumber.Name = "textBoxRegisterNursePhoneNumber";
            this.textBoxRegisterNursePhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNursePhoneNumber.TabIndex = 17;
            // 
            // textBoxRegisterNurseZipCode
            // 
            this.textBoxRegisterNurseZipCode.Location = new System.Drawing.Point(145, 216);
            this.textBoxRegisterNurseZipCode.Name = "textBoxRegisterNurseZipCode";
            this.textBoxRegisterNurseZipCode.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNurseZipCode.TabIndex = 16;
            // 
            // registerNurseLabel
            // 
            this.registerNurseLabel.AutoSize = true;
            this.registerNurseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.registerNurseLabel.Location = new System.Drawing.Point(8, 7);
            this.registerNurseLabel.Name = "registerNurseLabel";
            this.registerNurseLabel.Size = new System.Drawing.Size(153, 22);
            this.registerNurseLabel.TabIndex = 15;
            this.registerNurseLabel.Text = "Register a New Nurse";
            this.registerNurseLabel.UseCompatibleTextRendering = true;
            // 
            // labelLoggedIn
            // 
            this.labelLoggedIn.AutoSize = true;
            this.labelLoggedIn.Location = new System.Drawing.Point(494, 2);
            this.labelLoggedIn.Name = "labelLoggedIn";
            this.labelLoggedIn.Size = new System.Drawing.Size(58, 13);
            this.labelLoggedIn.TabIndex = 1;
            this.labelLoggedIn.Text = "Logged In:";
            // 
            // labelLoggedInName
            // 
            this.labelLoggedInName.AutoSize = true;
            this.labelLoggedInName.Location = new System.Drawing.Point(558, 2);
            this.labelLoggedInName.Name = "labelLoggedInName";
            this.labelLoggedInName.Size = new System.Drawing.Size(10, 13);
            this.labelLoggedInName.TabIndex = 2;
            this.labelLoggedInName.Text = "-";
            // 
            // linkLabelLogOut
            // 
            this.linkLabelLogOut.AutoSize = true;
            this.linkLabelLogOut.Location = new System.Drawing.Point(631, 2);
            this.linkLabelLogOut.Name = "linkLabelLogOut";
            this.linkLabelLogOut.Size = new System.Drawing.Size(45, 13);
            this.linkLabelLogOut.TabIndex = 3;
            this.linkLabelLogOut.TabStop = true;
            this.linkLabelLogOut.Text = "Log Out";
            this.linkLabelLogOut.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelLogOut_LinkClicked_1);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 373);
            this.Controls.Add(this.linkLabelLogOut);
            this.Controls.Add(this.labelLoggedInName);
            this.Controls.Add(this.labelLoggedIn);
            this.Controls.Add(this.tabControl);
            this.Name = "MainForm";
            this.Text = "Clinic";
            this.tabControl.ResumeLayout(false);
            this.tabPageLogin.ResumeLayout(false);
            this.tabPageLogin.PerformLayout();
            this.groupBoxAdminLogin.ResumeLayout(false);
            this.groupBoxAdminLogin.PerformLayout();
            this.tabPageRegister.ResumeLayout(false);
            this.tabPageRegister.PerformLayout();
            this.tabPageUpdate.ResumeLayout(false);
            this.tabPageUpdate.PerformLayout();
            this.groupBoxUpdatePatientInfo.ResumeLayout(false);
            this.groupBoxUpdatePatientInfo.PerformLayout();
            this.tabPageView.ResumeLayout(false);
            this.tabPageView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearch)).EndInit();
            this.tabPageNewAppointment.ResumeLayout(false);
            this.tabPageNewAppointment.PerformLayout();
            this.tabPageAppointment.ResumeLayout(false);
            this.tabPageAppointment.PerformLayout();
            this.groupBoxVitalSigns.ResumeLayout(false);
            this.groupBoxVitalSigns.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBPSystolic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBPDiastolic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBodyTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPulse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAppointment)).EndInit();
            this.tabPageTest.ResumeLayout(false);
            this.groupBoxOrderTest.ResumeLayout(false);
            this.groupBoxOrderTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderTests)).EndInit();
            this.groupBoxRecordTestResults.ResumeLayout(false);
            this.groupBoxRecordTestResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLabTest)).EndInit();
            this.tabPageDiagnose.ResumeLayout(false);
            this.groupBoxDiagnosis.ResumeLayout(false);
            this.groupBoxDiagnosis.PerformLayout();
            this.tabPageAdmin.ResumeLayout(false);
            this.tabPageAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmin)).EndInit();
            this.tabPageRegisterNurse.ResumeLayout(false);
            this.tabPageRegisterNurse.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageLogin;
        private System.Windows.Forms.TabPage tabPageRegister;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label labelDOB;
        private System.Windows.Forms.Label labellname;
        private System.Windows.Forms.Label labelfname;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxlname;
        private System.Windows.Forms.TextBox textBoxfname;
        private System.Windows.Forms.TabPage tabPageView;
        private System.Windows.Forms.DataGridView dataGridViewSearch;
        private System.Windows.Forms.Button buttonRegister;
        private System.Windows.Forms.Label labelRegister;
        private System.Windows.Forms.Label labelPhoneNumberError;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelLoggedIn;
        private System.Windows.Forms.Label labelLoggedInName;
        private System.Windows.Forms.LinkLabel linkLabelLogOut;
        private System.Windows.Forms.Label labelLoginFailed;
        private System.Windows.Forms.Label labelRegisterStatus;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label labelDatePickerSearchDOB;
        private System.Windows.Forms.DateTimePicker dateTimePickerSearch;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.TextBox textBoxSearchFullName;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.RadioButton radioButtonSearchByNameAndDate;
        private System.Windows.Forms.RadioButton radioButtonSearchByName;
        private System.Windows.Forms.TabPage tabPageNewAppointment;
        private System.Windows.Forms.Label labelPatientIDNewAppointment;
        private System.Windows.Forms.Button buttonScheduleAppointment;
        private System.Windows.Forms.RichTextBox richTextBoxAppointmentPurpose;
        private System.Windows.Forms.TextBox textBoxDoctorID;
        private System.Windows.Forms.TextBox textBoxPatientIDNewAppointment;
        private System.Windows.Forms.Label labelAppointmentPurpose;
        private System.Windows.Forms.Label labelDoctorIDAppointment;
        private System.Windows.Forms.Label labelNewAppointmentDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerNewAppointmentDate;

        private System.Windows.Forms.TabPage tabPageUpdate;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelUpdatePhoneNumberError;
        private System.Windows.Forms.Label labelUpdateStatus;
        private System.Windows.Forms.DateTimePicker dateTimePickerUpdateDOB;
        private System.Windows.Forms.TextBox textBoxUpdatePhoneNumber;
        private System.Windows.Forms.TextBox textBoxUpdateAddress;
        private System.Windows.Forms.TextBox textBoxUpdatelname;
        private System.Windows.Forms.TextBox textBoxUpdatefname;
        private System.Windows.Forms.Label labelUpdatePhoneNumber;
        private System.Windows.Forms.Label labelUpdateAddress;
        private System.Windows.Forms.Label labelUpdateDOB;
        private System.Windows.Forms.Label labelUpdatelname;
        private System.Windows.Forms.Label labelUpdatefname;
        private System.Windows.Forms.Label labelUpdate;
        private System.Windows.Forms.TextBox textBoxUpdateID;
        private System.Windows.Forms.Label labelUpdateID;

        private System.Windows.Forms.Label labelScheduleAppointmentStatus;
        private System.Windows.Forms.Button buttonUpdateSearch;
        private System.Windows.Forms.TextBox textBoxZip;
        private System.Windows.Forms.Label labelZip;
        private System.Windows.Forms.ComboBox comboBoxState;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.RadioButton radioButtonSearchByDate;
        private System.Windows.Forms.TextBox textBoxUpdateZip;
        private System.Windows.Forms.Label labelUpdateZip;
        private System.Windows.Forms.ComboBox comboBoxUpdateState;
        private System.Windows.Forms.TextBox textBoxUpdateCity;
        private System.Windows.Forms.Label labelUpdateState;
        private System.Windows.Forms.Label labelUpdateCity;
        private System.Windows.Forms.Label labelZipError;
        private System.Windows.Forms.Label labelUpdateZipError;
        private System.Windows.Forms.TabPage tabPageAppointment;
        private System.Windows.Forms.DateTimePicker dateTimePickerNewAppointmentTime;
        private System.Windows.Forms.Label labelNewAppointmentTime;
        private System.Windows.Forms.Label labelNewAppointment;
        private System.Windows.Forms.TabPage tabPageAdmin;
        private System.Windows.Forms.Label labelPatientIDAppointment;
        private System.Windows.Forms.Label labelRecordAppointment;
        private System.Windows.Forms.NumericUpDown numericUpDownPulse;
        private System.Windows.Forms.Label labelPulse;
        private System.Windows.Forms.NumericUpDown numericUpDownBodyTemp;
        private System.Windows.Forms.Label labelBodyTemp;
        private System.Windows.Forms.NumericUpDown numericUpDownBPDiastolic;
        private System.Windows.Forms.NumericUpDown numericUpDownBPSystolic;
        private System.Windows.Forms.Label labelBPSystolic;
        private System.Windows.Forms.Label labelBPDiastolic;
        private System.Windows.Forms.Label labelSymptoms;
        private System.Windows.Forms.Label labelAppointmentStatus;
        private System.Windows.Forms.RichTextBox richTextBoxSymptoms;
        private System.Windows.Forms.Button buttonSearchAppointment;
        private System.Windows.Forms.Button buttonRecord;
        private System.Windows.Forms.TextBox textBoxPatientIDAppointment;
        private System.Windows.Forms.DateTimePicker dateTimePickerAppointmentTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerAppointmentDate;
        private System.Windows.Forms.Label labelAppointmentTime;
        private System.Windows.Forms.Label labelAppointmentDate;
        private System.Windows.Forms.GroupBox groupBoxVitalSigns;
        private System.Windows.Forms.DataGridView dataGridViewAppointment;
        private System.Windows.Forms.GroupBox groupBoxUpdatePatientInfo;
        private System.Windows.Forms.Label labelQueryBox;
        private System.Windows.Forms.Button buttonGoPersonsSQL;
        private System.Windows.Forms.Label labelAdmin;
        private System.Windows.Forms.RichTextBox richTextBoxAdminSQL;
        private System.Windows.Forms.GroupBox groupBoxAdminLogin;
        private System.Windows.Forms.LinkLabel linkLabelAdminLogin;
        private System.Windows.Forms.LinkLabel linkLabelHideAdminLogin;
        private System.Windows.Forms.Button buttonAdminSubmit;
        private System.Windows.Forms.Label labelAdminPassword;
        private System.Windows.Forms.Label labelAdminUsername;
        private System.Windows.Forms.TextBox textBoxAdminPassword;
        private System.Windows.Forms.TextBox textBoxAdminUsername;
        private System.Windows.Forms.Button buttonDiagnose;
        private System.Windows.Forms.Label labelUpdateAppointmentStatus;
        private System.Windows.Forms.TabPage tabPageDiagnose;
        private System.Windows.Forms.RadioButton radioButtonFinalDiagnosis;
        private System.Windows.Forms.RadioButton radioButtonInitialDiagnosis;
        private System.Windows.Forms.RichTextBox richTextBoxDiagnosis;
        private System.Windows.Forms.GroupBox groupBoxDiagnosis;
        private System.Windows.Forms.Button buttonRecordDiagnosis;
        private System.Windows.Forms.Button buttonCancelDiagnosis;
        private System.Windows.Forms.Label labelDiagnosisStatus;
        private System.Windows.Forms.TabPage tabPageTest;
        private System.Windows.Forms.GroupBox groupBoxRecordTestResults;
        private System.Windows.Forms.Label labelTestName;
        private System.Windows.Forms.Label labelRecordLabResultsStatus;
        private System.Windows.Forms.Button buttonRecordTestResults;
        private System.Windows.Forms.Label labelTestResults;
        private System.Windows.Forms.Label labelSelectTest;
        private System.Windows.Forms.DataGridView dataGridViewLabTest;
        private System.Windows.Forms.RichTextBox richTextBoxTestResults;
        private System.Windows.Forms.Button buttonOrderRecordTest;
        private System.Windows.Forms.GroupBox groupBoxOrderTest;
        private System.Windows.Forms.Label labelOrderTestStatus;
        private System.Windows.Forms.Label labelOrderTest;
        private System.Windows.Forms.Button buttonOrderTest;
        private System.Windows.Forms.DataGridView dataGridViewOrderTests;
        private System.Windows.Forms.RadioButton radioButtonSearchByApptDate;
        private System.Windows.Forms.RadioButton radioButtonSearchByIDAndDate;
        private System.Windows.Forms.RadioButton radioButtonSearchByID;
        private System.Windows.Forms.Button buttonDoneTest;
        private System.Windows.Forms.Button buttonFinish;
        private System.Windows.Forms.TabPage tabPageRegisterNurse;
        private System.Windows.Forms.Label labelRegisterNurseStatus;
        private System.Windows.Forms.Label labelRegisterNurseState;
        private System.Windows.Forms.Label labelRegisterNurseZipCode;
        private System.Windows.Forms.Label labelRegisterNursePhoneNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelRegisterNurseAddress;
        private System.Windows.Forms.Label labelRegisterNurseDateOfBirth;
        private System.Windows.Forms.Label labelRegisterNurseCity;
        private System.Windows.Forms.Label labelRegisterNurseFirstName;
        private System.Windows.Forms.Label labelRegisterNurseLastName;
        private System.Windows.Forms.ComboBox comboBoxRegisterNurseState;
        private System.Windows.Forms.Button buttonRegisterNurse;
        private System.Windows.Forms.Label labelRegisterNursePhoneNumberError;
        private System.Windows.Forms.Label labelRegisterNurseZipCodeError;
        private System.Windows.Forms.DateTimePicker dateTimePickerRegisterNurse;
        private System.Windows.Forms.TextBox textBoxRegisterNurseLastName;
        private System.Windows.Forms.TextBox textBoxRegisterNurseFirstName;
        private System.Windows.Forms.TextBox textBoxRegisterNurseAddress;
        private System.Windows.Forms.TextBox textBoxRegisterNurseCity;
        private System.Windows.Forms.TextBox textBoxRegisterNursePhoneNumber;
        private System.Windows.Forms.TextBox textBoxRegisterNurseZipCode;
        private System.Windows.Forms.Label registerNurseLabel;
        private System.Windows.Forms.Label labelRegisterNursePassword;
        private System.Windows.Forms.TextBox textBoxRegisterNursePassword;
        private System.Windows.Forms.DataGridView dataGridViewAdmin;
    }
}

