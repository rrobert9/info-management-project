﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinic.Model;

namespace Clinic.DAL.Interfaces
{
    interface IRepository<T>
    {
        void add(T entity);

        T GetById(int id);

        IList<T> GetAll();

    }
}
