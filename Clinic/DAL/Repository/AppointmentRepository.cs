﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinic.DAL.Interfaces;
using Clinic.Model;
using MySql.Data.MySqlClient;

namespace Clinic.DAL.Repository
{
    class AppointmentRepository : IRepository<Appointment>
    {
        string connectionLabel = "server=160.10.23.6; port=3306; uid=cs3230f16h;" +
                              "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
                              "ConvertZeroDateTime=true;";

        public void add(Appointment entity)
        {
            var param2 = new MySqlParameter("@patientID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@doctorID", MySqlDbType.Int32);
            var param4 = new MySqlParameter("@date", MySqlDbType.DateTime);
            var param5 = new MySqlParameter("@purpose", MySqlDbType.Text);
            var param6 = new MySqlParameter("@nurseID", MySqlDbType.Int32);

            param2.Value = entity.PatientID;
            param3.Value = entity.DoctorID;
            param4.Value = entity.Date;
            param5.Value = entity.Purpose;
            param6.Value = entity.NurseID;


                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "insert into APPOINTMENT (patientid, doctorid, nurseid, date, purpose) values(@patientID, @doctorID, @nurseID, @date, @purpose)", conn))
                    {
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);
                        cmd.Parameters.Add(param4);
                        cmd.Parameters.Add(param5);
                        cmd.Parameters.Add(param6);

                        cmd.ExecuteNonQuery();
                    }

                    conn.Close();
                }
            
        }

        public bool UpdateInitialDiagnosis(Appointment appt)
        {
           
            var param1 = new MySqlParameter("@initialDiagnosis", MySqlDbType.VarChar);
            var param2 = new MySqlParameter("@patientID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@doctorID", MySqlDbType.Int32);
            var param4 = new MySqlParameter("@date", MySqlDbType.DateTime);


            param1.Value = appt.InitialDiagnosis;
            param2.Value = appt.PatientID;
            param3.Value = appt.DoctorID;
            param4.Value = appt.Date;

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "update APPOINTMENT set initialDiagnosis = @initialDiagnosis where patientid = @patientID and doctorid = @doctorID and date = @date", conn))
                {
                    if (true)
                    {
                        cmd.Parameters.Add(param1);
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);
                        cmd.Parameters.Add(param4);

                        cmd.ExecuteNonQuery();

                    }
                }
                conn.Close();
            }
            return true;
        }

        public bool UpdateFinalDiagnosis(Appointment appt)
        {

            var param1 = new MySqlParameter("@finalDiagnosis", MySqlDbType.VarChar);
            var param2 = new MySqlParameter("@patientID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@doctorID", MySqlDbType.Int32);
            var param4 = new MySqlParameter("@date", MySqlDbType.DateTime);


            param1.Value = appt.FinalDiagnosis;
            param2.Value = appt.PatientID;
            param3.Value = appt.DoctorID;
            param4.Value = appt.Date;

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "update APPOINTMENT set finalDiagnosis = @finalDiagnosis where patientid = @patientID and doctorid = @doctorID and date = @date", conn))
                {
                    if (true)
                    {
                        cmd.Parameters.Add(param1);
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);
                        cmd.Parameters.Add(param4);

                        cmd.ExecuteNonQuery();

                    }
                }
                conn.Close();
            }
            return true;
        }



        public bool UpdateAppointment(Appointment appt)
        {
            var param2 = new MySqlParameter("@patientID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@doctorID", MySqlDbType.Int32);
            var param4 = new MySqlParameter("@date", MySqlDbType.DateTime);
            var param5 = new MySqlParameter("@purpose", MySqlDbType.Text);

            var param6 = new MySqlParameter("@bpsystolic", MySqlDbType.Int32);
            var param7 = new MySqlParameter("@bpdiastolic", MySqlDbType.Int32);
            var param8 = new MySqlParameter("@bodytemp", MySqlDbType.Double);
            var param9 = new MySqlParameter("@pulse", MySqlDbType.Int32);
            var param10 = new MySqlParameter("@symptoms", MySqlDbType.Text);

            param2.Value = appt.PatientID;
            param3.Value = appt.DoctorID;
            param4.Value = appt.Date;
            param5.Value = appt.Purpose;

            param6.Value = appt.BPSystolic;
            param7.Value = appt.BPDiastolic;
            param8.Value = appt.BodyTemp;
            param9.Value = appt.Pulse;
            param10.Value = appt.Symptoms;

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "update APPOINTMENT set bpsystolic = @bpsystolic, bpdiastolic = @bpdiastolic, bodytemp = @bodytemp, pulse = @pulse, symptoms = @symptoms where patientid = @patientID and doctorid = @doctorID and date = @date", conn))
                {
                    if (true)
                    {
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);
                        cmd.Parameters.Add(param4);
                        cmd.Parameters.Add(param5);
                        cmd.Parameters.Add(param6);
                        cmd.Parameters.Add(param7);
                        cmd.Parameters.Add(param8);
                        cmd.Parameters.Add(param9);
                        cmd.Parameters.Add(param10);

                        cmd.ExecuteNonQuery();

                    }
                    else
                    {
                        return false;
                    }
                }
                conn.Close();
            }
            return true;
        }

        public IList<Appointment> GetByPatient(int patientID)
        {
            IList<Appointment> appointments = new List<Appointment>();
            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "select patientid, doctorid, date, purpose, bpsystolic, bpdiastolic, bodytemp, pulse, symptoms, id, nurseid from APPOINTMENT where patientid = ?patientID",
                            conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("patientID", patientID));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var appointment = new Appointment();

                            var purpose = checkForNullStrings(reader, 3, 4);
                            var symptoms = checkForNullStrings(reader, 8, 9);

                            appointment.PatientID = reader.GetInt32(0);
                            appointment.DoctorID = reader.GetInt32(1);
                            appointment.Date = reader.GetDateTime(2);
                            appointment.Purpose = purpose[0];

                            appointment.BPSystolic = reader.GetInt32(4);
                            appointment.BPDiastolic = reader.GetInt32(5);
                            appointment.BodyTemp = reader.GetDouble(6);
                            appointment.Pulse = reader.GetInt32(7);
                            appointment.Symptoms = symptoms[0];

                            appointment.ID = reader.GetInt32(9);
                            appointment.NurseID = reader.GetInt32(10);

                            appointments.Add(appointment);
                        }
                    }
                }
            }

            return appointments;
        }

        public IList<Appointment> GetByPatientAndDateTime(int patientID, DateTime appointmentDateTime)
        {
            IList<Appointment> appointments = new List<Appointment>();
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "select patientid, doctorid, date, purpose, bpsystolic, bpdiastolic, bodytemp, pulse, symptoms, id, nurseid from APPOINTMENT where patientid = ?patientID and date = ?appointmentDateTime",
                                conn))
                    {
                        cmd.Parameters.Add(new MySqlParameter("patientID", patientID));
                        cmd.Parameters.Add(new MySqlParameter("appointmentDateTime", appointmentDateTime));
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var appointment = new Appointment();

                                var purpose = checkForNullStrings(reader, 3, 4);
                                var symptoms = checkForNullStrings(reader, 8, 9);

                                appointment.PatientID = reader.GetInt32(0);
                                appointment.DoctorID = reader.GetInt32(1);
                                appointment.Date = reader.GetDateTime(2);
                                appointment.Purpose = purpose[0];

                                appointment.BPSystolic = reader.GetInt32(4);
                                appointment.BPDiastolic = reader.GetInt32(5);
                                appointment.BodyTemp = reader.GetDouble(6);
                                appointment.Pulse = reader.GetInt32(7);
                                appointment.Symptoms = symptoms[0];

                                appointment.ID = reader.GetInt32(9);
                                appointment.NurseID = reader.GetInt32(10);

                                 appointments.Add(appointment);
                            }
                        }
                    }
                }
            
            return appointments;
        }

        public IList<Appointment> GetByDateTime( DateTime appointmentDateTime)
        {
            IList<Appointment> appointments = new List<Appointment>();
            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "select patientid, doctorid, date, purpose, bpsystolic, bpdiastolic, bodytemp, pulse, symptoms, id, nurseid from APPOINTMENT where date = ?appointmentDateTime",
                            conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("appointmentDateTime", appointmentDateTime));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var appointment = new Appointment();

                            var purpose = checkForNullStrings(reader, 3, 4);
                            var symptoms = checkForNullStrings(reader, 8, 9);

                            appointment.PatientID = reader.GetInt32(0);
                            appointment.DoctorID = reader.GetInt32(1);
                            appointment.Date = reader.GetDateTime(2);
                            appointment.Purpose = purpose[0];

                            appointment.BPSystolic = reader.GetInt32(4);
                            appointment.BPDiastolic = reader.GetInt32(5);
                            appointment.BodyTemp = reader.GetDouble(6);
                            appointment.Pulse = reader.GetInt32(7);
                            appointment.Symptoms = symptoms[0];

                            appointment.ID = reader.GetInt32(9);
                            appointment.NurseID = reader.GetInt32(10);

                            appointments.Add(appointment);
                        }
                    }
                }
            }

            return appointments;
        }

        public Appointment GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<Appointment> GetAll()
        {
            throw new NotImplementedException();
        }

        private List<string> checkForNullStrings(MySqlDataReader reader, int startIndex, int endIndex)
        {
            var values = new List<string>();

            for (var i = startIndex; i < endIndex; i++)
            {
                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    values.Add(reader.GetString(i));
                }
            }

            return values;
        }
    }
}
