﻿using System;
using System.Collections.Generic;
using Clinic.DAL.Interfaces;
using Clinic.Model;
using MySql.Data.MySqlClient;

namespace Clinic.DAL.Repository
{
    class AdministratorRepository : IRepository<Administrator>
    {

        private readonly String connectionLabel;

        public AdministratorRepository()
        {
            this.connectionLabel = "server=160.10.23.6; port=3306; uid=cs3230f16h;" +
     "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
      "ConvertZeroDateTime=true;";
        }

        public void add(Administrator entity)
        {
            throw new NotImplementedException();
        }

        public Administrator GetByIdAndPassword(int id, byte[] password)
        {
            Administrator admin = new Administrator();
            using (MySqlConnection conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, password, bdate from ADMINISTRATOR where id = ?ID and password = ?PASSWORD", conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("PASSWORD", password));
                    cmd.Parameters.Add(new MySqlParameter("ID", id));
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var values = this.checkForNullStrings(reader, 1, 5);

                            admin.ID = reader.GetInt32(0);

                            admin.LastName = values[0];
                            admin.FirstName = values[1];
                            admin.Address = values[2];
                            admin.PhoneNumber = values[3];

                            admin.Password = (byte[]) reader.GetValue(5);
                            admin.DOB = reader.GetDateTime(6);
                        }
                    }
                }
            }
            
            

            return admin;
        }

        public IList<Administrator> GetAll()
        {
            IList<Administrator> admins = new List<Administrator>();
                using (MySqlConnection conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from ADMINISTRATOR", conn))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Administrator admin = new Administrator();

                                var values = this.checkForNullStrings(reader, 1, 5);

                                admin.ID = reader.GetInt32(0);

                                admin.LastName = values[0];
                                admin.FirstName = values[1];
                                admin.Address = values[2];
                                admin.PhoneNumber = values[3];
                            

                                admin.DOB = reader.GetDateTime(6);

                                admins.Add(admin);

                            }
                        }
                    }
                }
            

            return admins;
        }

        

        #region Check Methods

        private List<string> checkForNullStrings(MySqlDataReader reader, int startIndex, int endIndex)
        {
            List<string> values = new List<string>();

            for (int i = startIndex; i <= endIndex; i++)
            {
                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    values.Add(reader.GetString(i));
                }
            }

            return values;
        }
        Administrator IRepository<Administrator>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        IList<Administrator> IRepository<Administrator>.GetAll()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
