﻿using System;
using System.Collections.Generic;
using Clinic.DAL.Interfaces;
using Clinic.Model;
using MySql.Data.MySqlClient;

namespace Clinic.DAL.Repository
{
    class NurseRepository
    {

        private readonly String connectionLabel;
        private HashHelper hashHelper;

        public NurseRepository()
       {            this.connectionLabel = "server=160.10.23.6; port=3306; uid=cs3230f16h;" +
             "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
              "ConvertZeroDateTime=true;";

            this.hashHelper = new HashHelper();
        }

       
             public bool add(Nurse entity)
        {
            var param1 = new MySqlParameter("@fname", MySqlDbType.VarChar, 24);
            var param2 = new MySqlParameter("@lname", MySqlDbType.VarChar, 24);
            var param3 = new MySqlParameter("@bdate", MySqlDbType.DateTime);
            var param4 = new MySqlParameter("@address", MySqlDbType.VarChar, 40);
            var param5 = new MySqlParameter("@phoneNumber", MySqlDbType.VarChar, 10);
            var param6 = new MySqlParameter("@password", MySqlDbType.VarBinary, 20);
        
            

            param1.Value = entity.FirstName;
            param2.Value = entity.LastName;
            param3.Value = entity.DOB;
            param4.Value = entity.Address;
            param5.Value = entity.PhoneNumber;
            param6.Value = this.hashHelper.GetSHA1("Password");
           

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "insert into NURSE values(id, @fname, @lname, @bdate, @address, @phoneNumber, @password)", conn))
                {
                    if ((string)param1.Value != "" && (string)param2.Value != ""
                        && (string)param4.Value != "" && (string)param5.Value != "")
                    {
                        cmd.Parameters.Add(param1);
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);
                        cmd.Parameters.Add(param4);
                        cmd.Parameters.Add(param5);
                        cmd.Parameters.Add(param6);
                    

                        cmd.ExecuteNonQuery();
                    }
                }
                
                conn.Close();
                return true;
            }


    }

    public bool UpdateNurse(Nurse entity)
        {
            var param1 = new MySqlParameter("@id", MySqlDbType.Int32, 9);
            var param2 = new MySqlParameter("@fname", MySqlDbType.VarChar, 24);
            var param3 = new MySqlParameter("@lname", MySqlDbType.VarChar, 24);
            var param4 = new MySqlParameter("@bdate", MySqlDbType.DateTime);
            var param5 = new MySqlParameter("@address", MySqlDbType.VarChar, 40);
            var param6 = new MySqlParameter("@phoneNumber", MySqlDbType.VarChar, 10);
            var param7 = new MySqlParameter("@password", MySqlDbType.VarBinary);

            param1.Value = entity.ID;
            param2.Value = entity.FirstName;
            param3.Value = entity.LastName;
            param4.Value = entity.DOB;
            param5.Value = entity.Address;
            param6.Value = entity.PhoneNumber;
            param7.Value = entity.Password;

                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "update NURSE set fname = @fname, lname = @lname, bdate = @bdate, address = @address, phoneNumber = @phoneNumber, password = @password where id = @id", conn))
                    {
                        if ((string)param2.Value != "" && (string)param3.Value != ""
                            && (string)param5.Value != "" && (string)param6.Value != "")
                        {
                            cmd.Parameters.Add(param1);
                            cmd.Parameters.Add(param2);
                            cmd.Parameters.Add(param3);
                            cmd.Parameters.Add(param4);
                            cmd.Parameters.Add(param5);
                            cmd.Parameters.Add(param6);
                            cmd.Parameters.Add(param7);

                            cmd.ExecuteNonQuery();

                        }
                        else
                        {
                            return false;
                        }
                    }

                    conn.Close();

                }
           
            return true;
        }



        public Nurse GetByIdAndPassword(int id, byte[] password)
        {
            Nurse nurse = new Nurse();
            
                using (MySqlConnection conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, password, bdate from NURSE where id = ?ID and password = ?PASSWORD", conn))
                    {
                        cmd.Parameters.Add(new MySqlParameter("ID", id));
                        cmd.Parameters.Add(new MySqlParameter("PASSWORD", password));

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var values = this.checkForNullStrings(reader, 1, 5);

                                nurse.ID = reader.GetInt32(0);

                                nurse.LastName = values[0];
                                nurse.FirstName = values[1];
                                nurse.Address = values[2];
                                nurse.PhoneNumber = values[3];
                                
                                
                                
                                nurse.Password = (byte[]) reader.GetValue(5);
                                nurse.DOB = reader.GetDateTime(6);
                            }
                        }
                    }
                }
            
            

            return nurse;
        }

        public IList<Nurse> GetAll()
        {
            IList<Nurse> nurses = new List<Nurse>();
                using (MySqlConnection conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from NURSE", conn))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Nurse nurse = new Nurse();

                                var values = this.checkForNullStrings(reader, 1, 5);

                                nurse.ID = reader.GetInt32(0);

                                nurse.LastName = values[0];
                                nurse.FirstName = values[1];
                                nurse.Address = values[2];
                                nurse.PhoneNumber = values[3];
                                

                                nurse.DOB = reader.GetDateTime(6);

                                nurses.Add(nurse);

                            }
                        }
                    }
                }
          

            return nurses;
        }

        public IList<Nurse> GetByDOB(DateTime dob)
        {
            throw new NotImplementedException();
        }

        public IList<Nurse> GetByName(string fname, string lname)
        {
            throw new NotImplementedException();
        }

        public IList<Nurse> GetByDOBAndName(DateTime dob, string fname, string lname)
        {
            throw new NotImplementedException();
        }

        #region Check Methods

        private List<string> checkForNullStrings(MySqlDataReader reader, int startIndex, int endIndex)
        {
            List<string> values = new List<string>();

            for (int i = startIndex; i <= endIndex; i++)
            {
                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    values.Add(reader.GetString(i));
                }
            }

            return values;
        }

        #endregion

    }
}
