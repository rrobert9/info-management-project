﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinic.DAL.Interfaces;
using Clinic.Model;
using MySql.Data.MySqlClient;

namespace Clinic.DAL.Repository
{
    class TestRepository : IRepository<Test>
    {
        private readonly string connectionLabel;

        public TestRepository()
        {
            connectionLabel = "server=160.10.23.6; port=3306; uid=cs3230f16h;" +
                              "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
                              "ConvertZeroDateTime=true;";
        }

        public void add(Test entity)
        {
            throw new NotImplementedException();
        }

        public Test GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<Test> GetAll()
        {
            IList<Test> tests = new List<Test>();
            
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("select id, name from TEST",
                            conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var test = new Test();

                                var values = checkForNullStrings(reader, 1, 2);

                                test.ID = reader.GetInt32(0);

                                test.Name = values[0];

                                tests.Add(test);
                            }

                            conn.Close();
                        }
                    }
                }
            return tests;
        }

        public void order(Test test, int apptID)
        {
            var param2 = new MySqlParameter("@testID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@apptID", MySqlDbType.Int32);

            param2.Value = test.ID;
            param3.Value = apptID;

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "INSERT INTO HASTEST (appointmentid, testid) values(@apptID, @testid)", conn))
                {

                    cmd.Parameters.Add(param2);
                    cmd.Parameters.Add(param3);

                    cmd.ExecuteNonQuery();

                }
                conn.Close();
            }
        }

        public void updateTestResults(Test test, int apptID)
        {
            var param2 = new MySqlParameter("@testID", MySqlDbType.Int32);
            var param3 = new MySqlParameter("@apptID", MySqlDbType.Int32);
            var param4 = new MySqlParameter("@results", MySqlDbType.Text);

            param2.Value = test.ID;
            param3.Value = apptID;
            param4.Value = test.Results;

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "update HASTEST set results = @results where testid = @testID and appointmentid = @apptID", conn))
                {

                    cmd.Parameters.Add(param2);
                    cmd.Parameters.Add(param3);
                    cmd.Parameters.Add(param4);

                    cmd.ExecuteNonQuery();
                    
                }
                conn.Close();
            }
        }

        public IList<Test> GetAllOrderedTests(int apptID)
        {
            IList<Test> tests = new List<Test>();

            using (var conn = new MySqlConnection(connectionLabel))
            {
                conn.Open();

                using (
                    var cmd = new MySqlCommand("select ht.testid, t.name, ht.results from HASTEST ht, TEST t where ht.appointmentid = ?apptid and ht.testid = t.id",
                        conn))
                {
                    cmd.Parameters.Add(new MySqlParameter("apptID", apptID));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var test = new Test();

                            var values = checkForNullStrings(reader, 1, 3);

                            test.ID = reader.GetInt32(0);
                            test.Name = values[0];
                            test.Results = values[1];

                            tests.Add(test);
                        }

                        conn.Close();
                    }
                }
            }
            return tests;
        }

        private List<string> checkForNullStrings(MySqlDataReader reader, int startIndex, int endIndex)
        {
            var values = new List<string>();

            for (var i = startIndex; i < endIndex; i++)
            {
                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    values.Add(reader.GetString(i));
                }
            }

            return values;
        }
    }
}
