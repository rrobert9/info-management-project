﻿using System;
using System.Collections.Generic;
using Clinic.DAL.Interfaces;
using Clinic.Model;
using MySql.Data.MySqlClient;

namespace Clinic.DAL.Repository
{
    internal class PatientRepository : IRepository<Patient>
    {
        private readonly string connectionLabel;

        public PatientRepository()
        {
            connectionLabel = "server=160.10.23.6; port=3306; uid=cs3230f16h;" +
                              "pwd=rJT2wA7wH3YN6A4e; database=cs3230f16h;" +
                              "ConvertZeroDateTime=true;";
        }

        public void add(Patient entity)
        {
            var param2 = new MySqlParameter("@fname", MySqlDbType.VarChar, 24);
            var param3 = new MySqlParameter("@lname", MySqlDbType.VarChar, 24);
            var param4 = new MySqlParameter("@bdate", MySqlDbType.DateTime);
            var param5 = new MySqlParameter("@address", MySqlDbType.VarChar, 40);
            var param6 = new MySqlParameter("@phoneNumber", MySqlDbType.VarChar, 10);

            param2.Value = entity.FirstName;
            param3.Value = entity.LastName;
            param4.Value = entity.DOB;
            param5.Value = entity.Address;
            param6.Value = entity.PhoneNumber;

                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "insert into PATIENT values(id, @fname, @lname, @bdate, @address, @phoneNumber)", conn))
                    {
                        if ((string) param2.Value != "" && (string) param3.Value != ""
                            && (string) param5.Value != "" && (string) param6.Value != "")
                        {
                            cmd.Parameters.Add(param2);
                            cmd.Parameters.Add(param3);
                            cmd.Parameters.Add(param4);
                            cmd.Parameters.Add(param5);
                            cmd.Parameters.Add(param6);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    conn.Close();
                }
            
        }

        public bool UpdatePatient(Patient entity)
        {
            var param1 = new MySqlParameter("@id", MySqlDbType.Int32, 9);
            var param2 = new MySqlParameter("@fname", MySqlDbType.VarChar, 24);
            var param3 = new MySqlParameter("@lname", MySqlDbType.VarChar, 24);
            var param4 = new MySqlParameter("@bdate", MySqlDbType.DateTime);
            var param5 = new MySqlParameter("@address", MySqlDbType.VarChar, 40);
            var param6 = new MySqlParameter("@phoneNumber", MySqlDbType.VarChar, 10);

            param1.Value = entity.ID;
            param2.Value = entity.FirstName;
            param3.Value = entity.LastName;
            param4.Value = entity.DOB;
            param5.Value = entity.Address;
            param6.Value = entity.PhoneNumber;

                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "update PATIENT set fname = @fname, lname = @lname, bdate = @bdate, address = @address, phoneNumber = @phoneNumber where id = @id", conn))
                    {
                        if ((string)param2.Value != "" && (string)param3.Value != ""
                            && (string)param5.Value != "" && (string)param6.Value != "")
                        {
                            cmd.Parameters.Add(param1);
                            cmd.Parameters.Add(param2);
                            cmd.Parameters.Add(param3);
                            cmd.Parameters.Add(param4);
                            cmd.Parameters.Add(param5);
                            cmd.Parameters.Add(param6);

                            cmd.ExecuteNonQuery();
                            
                        } else
                        {
                            return false;
                        }
                    }

                    conn.Close();
                    
                }
            
            return true; 
        }
    

        public Patient GetById(int id)
        {
            var patient = new Patient();
            
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "select id, lname, fname, address, phoneNumber, bdate from PATIENT where id = ?ID", conn))
                    {
                        cmd.Parameters.Add(new MySqlParameter("ID", id));
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var values = checkForNullStrings(reader, 1, 5);

                                patient.ID = reader.GetInt32(0);

                                patient.LastName = values[0];
                                patient.FirstName = values[1];
                                patient.Address = values[2];
                                patient.PhoneNumber = values[3];

                                patient.DOB = reader.GetDateTime(5);
                            }
                        }
                    }
                }
            

            return patient;
        }

        public IList<Patient> GetAll()
        {
            IList<Patient> patients = new List<Patient>();
            
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from PATIENT",
                            conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var patient = new Patient();

                                var values = checkForNullStrings(reader, 1, 5);

                                patient.ID = reader.GetInt32(0);

                                patient.LastName = values[0];
                                patient.FirstName = values[1];
                                patient.Address = values[2];
                                patient.PhoneNumber = values[3];

                                patient.DOB = reader.GetDateTime(5);

                                patients.Add(patient);
                            }

                            conn.Close();
                        }
                    }
                }
                
            

            return patients;
        }

        public IList<Patient> GetByDOB(DateTime dob)
        {
            IList<Patient> patients = new List<Patient>();
            var param1 = new MySqlParameter("@bdate", MySqlDbType.DateTime);

            param1.Value = dob.Date;

           
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from PATIENT where bdate = @bdate", conn))
                    {
                        cmd.Parameters.Add(param1);

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var patient = new Patient();

                                var values = checkForNullStrings(reader, 1, 5);

                                patient.ID = reader.GetInt32(0);

                                patient.LastName = values[0];
                                patient.FirstName = values[1];
                                patient.Address = values[2];
                                patient.PhoneNumber = values[3];
                                patient.DOB = reader.GetDateTime(5);

                                patients.Add(patient);

                            }
                        }
                    }

                    conn.Close();
                }
            

            return patients;
        }

        public IList<Patient> GetByDOBAndName(DateTime dob, string fname, string lname)
        {
            IList<Patient> patients = new List<Patient>();
            var param1 = new MySqlParameter("@bdate", MySqlDbType.DateTime);
            var param2 = new MySqlParameter("@fname", MySqlDbType.VarChar);
            var param3 = new MySqlParameter("@lname", MySqlDbType.VarChar);

            param1.Value = dob;
            param2.Value = fname;
            param3.Value = lname;

            
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from PATIENT where bdate = @bdate and fname = @fname and lname = @lname", conn))
                    {
                        cmd.Parameters.Add(param1);
                        cmd.Parameters.Add(param2);
                        cmd.Parameters.Add(param3);

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var patient = new Patient();

                                var values = checkForNullStrings(reader, 1, 5);

                                patient.ID = reader.GetInt32(0);

                                patient.LastName = values[0];
                                patient.FirstName = values[1];
                                patient.Address = values[2];
                                patient.PhoneNumber = values[3];
                                patient.DOB = reader.GetDateTime(5);

                                patients.Add(patient);

                            }
                        }
                    }

                    conn.Close();
                }
          

            return patients;
        }

        public IList<Patient> GetByName(string fname, string lname)
        {
            IList<Patient> patients = new List<Patient>();
            var param1 = new MySqlParameter("@fname", MySqlDbType.VarChar);
            var param2 = new MySqlParameter("@lname", MySqlDbType.VarChar);

            param1.Value = fname;
            param2.Value = lname;

            
                using (var conn = new MySqlConnection(connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("select id, lname, fname, address, phoneNumber, bdate from PATIENT where fname = @fname and lname = @lname", conn))
                    {
                        cmd.Parameters.Add(param1);
                        cmd.Parameters.Add(param2);

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var patient = new Patient();

                                var values = checkForNullStrings(reader, 1, 5);

                                patient.ID = reader.GetInt32(0);

                                patient.LastName = values[0];
                                patient.FirstName = values[1];
                                patient.Address = values[2];
                                patient.PhoneNumber = values[3];
                                patient.DOB = reader.GetDateTime(5);

                                patients.Add(patient);

                            }
                        }
                    }

                    conn.Close();
                }
           

            return patients;
        }

        #region Check Methods

        private List<string> checkForNullStrings(MySqlDataReader reader, int startIndex, int endIndex)
        {
            var values = new List<string>();

            for (var i = startIndex; i < endIndex; i++)
            {
                if (reader.IsDBNull(i))
                {
                    values.Add("NULL");
                }
                else
                {
                    values.Add(reader.GetString(i));
                }
            }

            return values;
        }

        #endregion
    }
}