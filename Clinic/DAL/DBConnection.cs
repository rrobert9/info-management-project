﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace Clinic.DAL
{
    class DBConnection
    {

        public static MySqlConnection GetConnection(String connlabel)
        {
            String connStrLabel = "MySqlDbConnection";
            String connStr = ConfigurationManager.ConnectionStrings[connStrLabel].ConnectionString;

            return new MySqlConnection(connStr);
        }

    }
}
