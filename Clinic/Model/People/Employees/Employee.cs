﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic.Model.People
{
    public class Employee : Person
    {
        private byte[] password;

        public byte[] Password
        {
            get { return password; }
            set { password = value; }
        }
    }
}
