﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinic.Model
{
    class Appointment
    {
        private int id;
        private int patientID;
        private int doctorID;
        private int nurseID;

        private DateTime date;
        private string purpose;

        private int bpSystolic;
        private int bpDiastolic;
        private double bodyTemp;
        private int pulse;
        private string symptoms;

        private string initialDiagnosis;
        private string finalDiagnosis;

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public int PatientID
        {
            get { return this.patientID; }
            set { this.patientID = value; }
        }

        public int DoctorID
        {
            get { return this.doctorID; }
            set { this.doctorID = value; }
        }

        public int NurseID
        {
            get { return this.nurseID; }
            set { this.nurseID = value; }
        }

        public DateTime Date
        {
            get { return this.date; }
            set { this.date = value; }
        }

        public string Purpose
        {
            get { return this.purpose; }
            set { this.purpose = value; }
        }

        public int BPSystolic
        {
            get { return this.bpSystolic; }
            set { this.bpSystolic = value; }
        }

        public int BPDiastolic
        {
            get { return this.bpDiastolic; }
            set { this.bpDiastolic = value; }
        }

        public double BodyTemp
        {
            get { return this.bodyTemp; }
            set { this.bodyTemp = value; }
        }

        public int Pulse
        {
            get { return this.pulse; }
            set { this.pulse = value; }
        }

        public string Symptoms
        {
            get { return this.symptoms; }
            set { this.symptoms = value; }
        }

        public string InitialDiagnosis
        {
            get { return this.initialDiagnosis; }
            set { this.initialDiagnosis = value; }
        }

        public string FinalDiagnosis
        {
            get { return this.finalDiagnosis; }
            set { this.finalDiagnosis = value; }
        }
    }
}
