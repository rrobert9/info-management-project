﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using Clinic.DAL.Interfaces;
using Clinic.DAL.Repository;
using Clinic.Model;
using Clinic.Model.People;

namespace Clinic.Controller
{
    class ClinicController
    {
        private NurseRepository nurseRep;
        private AdministratorRepository adminRep;
        private PatientRepository patientRep;
        private AppointmentRepository apptRep;
        private TestRepository testRep;

        public ClinicController()
        {
            this.nurseRep = new NurseRepository();
            this.patientRep = new PatientRepository();
            this.apptRep = new AppointmentRepository();
            this.adminRep = new AdministratorRepository();
            this.testRep = new TestRepository();
        }

        public ClinicController(IRepository<Nurse> rep)
        {
            this.nurseRep = new NurseRepository();
        }

        public Employee login(int userID, byte[] password)
        {
            //log in using an employeeRepository here to scan all employee IDs instead
            Employee loggedInEmployee = this.nurseRep.GetByIdAndPassword(userID, password);
            return loggedInEmployee;
        }

        public Employee loginAdmin(int userID, byte[] password)
        {
            //log in using an employeeRepository here to scan all employee IDs instead
            Employee loggedInEmployee = this.adminRep.GetByIdAndPassword(userID, password);
            return loggedInEmployee;
        }

        public void addPatient(Patient entity)
        {
            this.patientRep.add(entity);

          
        }

        public void addApointment(Appointment appointment)
        {
            this.apptRep.add(appointment);

        }

        public bool addNurse(Nurse nurse)
        {
           return this.nurseRep.add(nurse);
        }

        public bool updateNurse(Nurse nurse)
        {
            return this.nurseRep.UpdateNurse(nurse);
        }

        public IList<Patient> GetPatientByDOB(DateTime dob)
        {
            return this.patientRep.GetByDOB(dob);
        }

        public IList<Patient> GetPatientByName(string fname, string lname)
        {
            return this.patientRep.GetByName(fname, lname);
        }

        public IList<Patient> GetPatientByDOBAndName(DateTime dob, string fname, string lname)
        {
            return this.patientRep.GetByDOBAndName(dob, fname, lname);
        }

        public Patient GetPatientByID(int id)
        {
            return this.patientRep.GetById(id);
        }

        public bool UpdatePatient(Patient entity)
        {
            return this.patientRep.UpdatePatient(entity);
        }

        public bool UpdateAppointment(Appointment entity)
        {
            return this.apptRep.UpdateAppointment(entity);
        }

        public bool UpdateAppointmentInitialDiagnosis(Appointment entity)
        {
            return this.apptRep.UpdateInitialDiagnosis(entity);
        }

        public bool UpdateAppointmentFinalDiagnosis(Appointment entity)
        {
            return this.apptRep.UpdateFinalDiagnosis(entity);
        }

        public IList<Appointment> GetAppointmentByPatientID(int patientID)
        {
            return this.apptRep.GetByPatient(patientID);
        } 

        public IList<Appointment> GetAppointmentByPatientIDAndDate(int patientID, DateTime appointmentDateTime)
        {
            return this.apptRep.GetByPatientAndDateTime(patientID, appointmentDateTime);
        }

        public IList<Appointment> GetAppointmentByDate( DateTime appointmentDateTime)
        {
            return this.apptRep.GetByDateTime(appointmentDateTime);
        }

        public IList<Test> GetAllTests()
        {
            return this.testRep.GetAll();
        }

        public IList<Test> GetAllOrderedTests(int apptID)
        {
            return this.testRep.GetAllOrderedTests(apptID);
        }

        public void UpdateTestResults(Test test, int apptID)
        {
            this.testRep.updateTestResults(test, apptID);
        }

        public void OrderTest(Test test, int apptID)
        {
            this.testRep.order(test, apptID);
        }

    }
}
