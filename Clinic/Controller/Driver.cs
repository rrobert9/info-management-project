﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinic.View;

namespace Clinic.Controller
{
    class Driver
    {
        private ClinicView viewer;

        public Driver()
        {
            ClinicController controller = new ClinicController();
            this.viewer = new ClinicView(controller);
        }

        public ClinicView getView()
        {
            return this.viewer;
        }


    }
}
